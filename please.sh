#! /bin/sh
set -e

say () { printf "fletimo: $@\n" >&2 ; } 

babuso_test_port=8480

build () {
    say "Building"
    dune build @check
    # bb self-test
    say "Building TZComet website"
    dune build @babuso-lib/runtest --auto
    dune runtest
    profile=dev root=tzcomet/ tzcomet/please.sh build all
    curl http://localhost:$babuso_test_port/please/die || echo "  \\-> don't care"
}

export BABUSO_ROOT=$PWD/_build/test-data-dir/
babusotest () {
    export OCAMLRUNPARAM=b
    while { 
        bb config --port $babuso_test_port --octez-client ../octez/tezos-client \
           --octez-node http://localhost:20000 \
           --theme random \
           --fake-octez-client \
           $ldiejdliej
        #--octez-node https://ithacanet.smartpy.io \
            #for f in $(find "$BABUSO_ROOT" -type f) ; do
        #    echo "$f:"
        #    cat "$f" | sed 's/^/  |/'
        #done
        bb self-test && bb start
    } do
          echo "==================== RESTARTING BABUSO $(date -Ins) ================"
    done
}
babi () {
    export BABUSO_ROOT=$PWD/_build/test-ithacanet-data-dir/
    bb config --port $babuso_test_port --octez-client ../octez/tezos-client \
       --octez-node https://ithacanet.smartpy.io \
       --theme default
    bb start
}


test () {
    dune runtest
}

deps () {
    if [ -d _opam ] ; then
        say 'Opam switch already there'
    else
        opam switch create fletimo-413 \
             --formula='"ocaml-base-compiler" {>= "4.13" & < "4.14"}'
        opam switch link fletimo-413 .
    fi
    eval $(opam env)
    opam pin add -n ocamlformat 0.19.0
    # TZComet-pins:
    opam pin add -n digestif 0.9.0
    opam pin add -n tyxml 4.5.0
    opam pin add -n zarith 1.11 # zarith_stubs_js fails with 1.12
    # Flextesa:
    opam install --yes --deps-only --with-test --with-doc \
         ./flextesa/tezai-base58-digest.opam ./flextesa/tezai-tz1-crypto.opam \
         ./flextesa/flextesa.opam ./flextesa/flextesa-cli.opam \
    # TZComet-installs:
    opam install -y base fmt uri cmdliner ezjsonm \
         ocamlformat uri merlin ppx_deriving angstrom \
         ppx_inline_test lwt-canceler.0.3 zarith_stubs_js \
         digestif tyxml tezos-micheline \
         js_of_ocaml-compiler js_of_ocaml-lwt
    # Babuso Installs:
    opam install tiny_httpd_camlzip
}

lint () {
    dune exec autifest/autifest.exe apply && dune build @fmt --auto
}

bb () { dune exec babuso-bin/babuso.exe -- "$@" ; }

reload () { eval $(./please.sh env) ; }

# __NOT_IN_ENV_FROM_HERE__
env () {
    opam env
    endline=`awk '/__NOT_IN_ENV_FROM_HERE__/ { print NR; exit 0; }' ./please.sh`
    head -n $endline ./please.sh | grep -v 'set -e' > /tmp/env.sh
    # eval $( ... ) swallows newlines, so we go through a temp-file:
    echo ". /tmp/env.sh"
}



{ if [ "$1" = "" ] ; then build ; else "$@" ; fi ; }
