open! Import
open SmartML
open Smart_ml
module L = struct include Literal end

module E = struct
  let none = unline Expr.none
  let sender = unline Expr.sender
  let amount = unline Expr.amount
  let constant = unline Expr.cst
  let bin_op op = unline (Expr.bin_op ~op)

  let optionally_get_field from = function
    | None -> from
    | Some name -> Expr.(attr ~line_no ~name from)

  let parameter ?field () =
    let base = unline Expr.params in
    optionally_get_field base field

  let storage ?field () =
    let base = unline Expr.storage in
    optionally_get_field base field

  let type_annotation ~t = unline (Expr.type_annotation ~t)

  (* (type_annotation ~line_no ~t:tparameter )) *)
  let typed_comb_record l =
    let t =
      Type.(
        record_default_layout SmartML.Config.Comb
          (List.map l ~f:(fun (f, t, _) -> (f, t)))) in
    type_annotation ~t
      Expr.(record ~line_no (List.map l ~f:(fun (f, _, v) -> (f, v))))

  let ( === ) a b = bin_op BEq a b
  let ( <== ) a b = bin_op BLe a b
end

module I = struct
  let seq = Smart_ml.unline Command.seq
  let set_delegate = Smart_ml.unline Library.set_delegate
  let send_mutez ~destination = unline Library.send destination
  let verify ?failwith expr = Smart_ml.unline Command.verify expr failwith
end

module T = struct
  let unit = Type.unit
  let key_hash = Type.key_hash
  let signature = Type.signature
  let operation = Type.operation
  let lambda a b = Type.(lambda no_effects a b)
  let bytes = Type.bytes
  let key = Type.key
  let address = Type.address
  let mutez = Type.token
  let nat = Type.nat ()
  let option = Type.option
  let list = Type.list
  let comb_record l = Type.(record_default_layout SmartML.Config.Comb l)
  let equal = Type.equal
  let compare = Type.compare
  let pp = Type.pp
end

let entry_point ?(parameter = T.unit) name cmds =
  let open SmartML in
  let open Basics in
  build_entry_point ~name ~tparameter:parameter I.(seq cmds)
