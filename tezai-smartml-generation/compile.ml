open! Import

let default_type_checking_config = SmartML.Config.default

type untyped_entrypoint =
  ( SmartML.Basics.Untyped.command
  , SmartML.Type.t
  , SmartML.Basics.untyped )
  SmartML.Basics.entry_point

let contract ?(michelson_simplification = true)
    ?(type_checking_simplify = default_type_checking_config.simplify)
    ?(storage_record = []) (entrypoints : untyped_entrypoint list) =
  let open SmartML in
  let storage, storage_literal, storage_fields_order =
    let open Construct in
    let make_one (f, t, l) = (f, t, E.constant l) in
    let lit l =
      Tools.Compiler.compile_literal l |> Tools.Michelson.string_of_literal
    in
    let lit_one (_, _, l) = lit l in
    let lit_list l = String.concat ~sep:" ; " l |> Fmt.str "{ %s }" in
    let field (f, _, _) = f in
    match storage_record with
    | [] -> (E.constant L.unit, lit L.unit, [])
    | [one] ->
        ( E.typed_comb_record
            [make_one one; ("dummy", T.unit, E.constant L.unit)]
        , lit_list [lit_one one; lit L.unit]
        , [field one; "dummy"] )
    | more ->
        ( E.typed_comb_record (List.map ~f:make_one more)
        , lit_list (List.map more ~f:lit_one)
        , List.map ~f:field more ) in
  let type_checked =
    let open Basics in
    let entry_points_layout =
      let open Utils_pure.Binary_tree in
      right_comb
        (List.map entrypoints ~f:(fun (ep : _ entry_point) ->
             Layout.{source= ep.channel; target= ep.channel} ) ) in
    let contract =
      let flags =
        if michelson_simplification then (* the default *) []
        else [Config.(Bool_Flag (Simplify, false))] in
      build_contract ~storage ~entry_points_layout entrypoints ~flags in
    let config = Config.{default with simplify= type_checking_simplify} in
    Tools.Checker.check_contract config contract in
  let instance =
    SmartML.Basics.
      { template= type_checked
      ; state=
          { balance= Utils_pure.Bigint.of_int 100
          ; storage= None
          ; baker= None
          ; lazy_entry_points= []
          ; metadata= [] } } in
  let compiled =
    Tools.Compiler.compile_instance ~scenario_vars:Utils.String.Map.empty
      instance in
  let concrete = Tools.Michelson.display_tcontract compiled in
  ( `Code concrete
  , `Init_example storage_literal
  , `Storage_fields_order storage_fields_order )
