open! Import
open Wallet_data

module Message = struct
  module Up = struct
    type t =
      | List_connected_ledgers
      | Import_account of
          { name: string
          ; kind:
              [ `Ledger_uri of string
              | `Unencrypted_uri of string
              | `Address of string ] }
      | Create_generic_multisig_account of
          {name: string; keys: string list; threshold: int; gas_wallet: string}
      | Create_smart_contract of
          { name: string
          ; gas_wallet: string
          ; entrypoints: Entrypoint.t list
          ; initialization: Variable.Map.t }
      | Order_operation of {specification: Operation.Specification.t}
      | Make_blob_for_signature of
          { target_contract_id: string
          ; entrypoint: string
          ; parameters: (string * Variable.Value.t) list }
      | Sign_blob of {account: string; blob: string}    
      | Delete_account of string
      | Get_configuration
      | Get_exchange_rates
      | All_accounts
      | All_operations
      | Get_events of float
    [@@deriving sexp, variants]
  end

  module Down = struct
    type ledger = {animals: string; wallet_app_version: string option}
    [@@deriving sexp, fields]

    let ledger = Fields_of_ledger.create

    module Alert = struct
      type content =
        | Ledger_wants_human_intervention of
            {ledger_id: string; ledger_name: string; reason: string}
      [@@deriving sexp, variants]

      type t = {id: string; content: content} [@@deriving sexp, fields, make]
    end

    type event =
      | Please_just_reload_all of string
      | New_exchange_rates of Exchange_rates.t
      | Alert_on of Alert.t
      | Alert_off of {id: string}
    [@@deriving sexp, variants]

    type t =
      | Ledgers of ledger list
      | Account_list of Account.t list
      | Operation_list of Operation.t list
      | Events of event list * float
      | Exchange_rates of Exchange_rates.t option
      | Blob of string
      | Signature of string
      | Done
    [@@deriving sexp, variants]
  end
end
