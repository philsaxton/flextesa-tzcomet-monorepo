open! Import
module B58 = struct type t = string [@@deriving sexp] end
module Address = struct type t = B58.t [@@deriving sexp] end
module Public_key = struct type t = B58.t [@@deriving sexp] end
module Public_key_hash = struct type t = B58.t [@@deriving sexp] end
module Signature = struct type t = B58.t [@@deriving sexp] end

module Z = struct
  include Z

  let sexp_of_t z = Sexp.(List [Atom "Z"; Atom (Z.to_string z)])

  let t_of_sexp sexp =
    match sexp with
    | Sexp.(List [Atom "Z"; Atom s]) -> Z.of_string s
    | _ -> assert false
end

module Mutez = struct
  include Z

  let to_tez_string mu =
    assert (Z.compare mu Z.zero >= 0) ;
    (mu / of_int 1_000_000 |> to_string)
    ^
    match mu mod of_int 1_000_000 |> to_int with
    | 0 -> ""
    | more -> Fmt.str ".%06d" more
end

module Variable = struct
  module Name = struct type t = string [@@deriving sexp] end

  module Type = struct
    type t =
      | Mutez
      | Address
      | Nat
      | Key_list
      | Pkh_option
      | Bytes
      | Signature_option_list
      | Lambda_unit_to_operation_list
    [@@deriving sexp, variants, compare, equal]
  end

  module Value = struct
    type t =
      | Address of Address.t
      | Mutez of Z.t
      | Nat of Z.t
      | Key_list of Public_key.t list
      | Unit
      | Some_pkh of Public_key_hash.t
      | Signature_option_list of Signature.t option list
      | Bytes of string
      | None_pkh
      | Lambda_unit_to_operation_list of string
    [@@deriving sexp, variants]

    let to_michelson = function
      | Unit -> "Unit"
      | None_pkh -> "None"
      | Some_pkh a -> Fmt.str "Some %S" a
      | Address a -> Fmt.str "%S" a
      | Bytes s ->
          let (`Hex h) = Hex.of_string s in
          Fmt.str "0x%s" h
      | Nat n | Mutez n -> Z.to_string n
      | Lambda_unit_to_operation_list s -> s
      (* | Comb l ->
          Fmt.str "{ %s }"
            (List.map ~f:to_michelson l |> String.concat ~sep:" ; ") *)
      | Signature_option_list kl ->
          Fmt.str "{ %s }"
            ( List.map
                ~f:(function None -> "None" | Some s -> Fmt.str "Some %S" s)
                kl
            |> String.concat ~sep:" ; " )
      | Key_list kl ->
          Fmt.str "{ %s }"
            (List.map ~f:(Fmt.str "%S") kl |> String.concat ~sep:" ; ")
  end

  module Map = struct
    type t = (Name.t * Value.t) list [@@deriving sexp]

    let empty : t = []
    let of_list = Fn.id

    let get_exn self = function
      | "dummy" -> Value.unit
      | k -> (
        try List.Assoc.find_exn self k ~equal:String.equal
        with _ -> Failure.raise_textf "Cannot find value for variable %S" k )
  end

  module Record = struct
    type t = Comb of (Name.t * Value.t) list [@@deriving sexp, variants]

    let empty = Comb []

    let of_map map ~structure : t =
      let f one = (one, Map.get_exn map one) in
      match structure with
      | [] -> Comb []
      | [one] -> Comb [f one]
      | more -> Comb (List.map ~f more)

    let to_michelson self =
      let f (_, one) = Value.to_michelson one in
      match self with
      | Comb [] -> "Unit"
      | Comb [one] -> f one
      | Comb more ->
          Fmt.str "{ %s }" (String.concat ~sep:" ; " (List.map ~f more))

    let get (Comb record) field =
      match List.Assoc.find record field ~equal:String.equal with
      | Some s -> s
      | None -> Fmt.failwith "Cannot find value for field %S" field
  end
end

module Constant_or_variable = struct
  type 'a t = Constant of 'a | Variable of Variable.Name.t
  [@@deriving sexp, variants]

  let collect_variables t = function Constant _ -> [] | Variable v -> [(v, t)]
end

module Control = struct
  type t =
    | Anyone
    | Weighted_multisig of
        { counter: Variable.Name.t
        ; threshold: Z.t Constant_or_variable.t
        ; keys: (Z.t * Public_key.t) list Constant_or_variable.t }
    | Sender_is of Address.t Constant_or_variable.t
    | When_inactive of {timespan: int Constant_or_variable.t; control: t}
    | And of t list
  [@@deriving sexp, variants]

  let rec collect_variables = function
    | Weighted_multisig {counter; threshold; keys} ->
        (counter, Variable.Type.nat)
        :: Constant_or_variable.collect_variables Variable.Type.nat threshold
        @ Constant_or_variable.collect_variables Variable.Type.key_list keys
    | Sender_is s ->
        Constant_or_variable.collect_variables Variable.Type.address s
    | When_inactive {timespan; control} ->
        Constant_or_variable.collect_variables Variable.Type.nat timespan
        @ collect_variables control
    | And l -> List.concat_map l ~f:collect_variables
    | Anyone -> []
end

module Ability = struct
  type t =
    | Call_operations_lambda
    | Transfer_mutez of
        { amount: [`No_limit | `Limit of Mutez.t Constant_or_variable.t]
        ; destination:
            [`Anyone | `Address of Address.t Constant_or_variable.t | `Sender]
        }
    | Set_delegate
    | Ping
    | Vote
    | Do_nothing
    | Booemrang
    | Update_variables of Variable.Name.t list
    | Generic_multisig_main
  [@@deriving sexp, variants]

  let collect_variables = function
    | Transfer_mutez {amount; destination} -> (
        ( match amount with
        | `No_limit -> []
        | `Limit l ->
            Constant_or_variable.collect_variables Variable.Type.mutez l )
        @
        match destination with
        | `Anyone | `Sender -> []
        | `Address add ->
            Constant_or_variable.collect_variables Variable.Type.address add )
    | Call_operations_lambda | Set_delegate | Ping | Vote | Do_nothing
     |Booemrang | Update_variables _ | Generic_multisig_main ->
        []

  let collect_parameters = function
    | Set_delegate -> [("delegate", Variable.Type.pkh_option)]
    | Transfer_mutez {amount= _; destination} ->
        ( match destination with
        | `Anyone -> [("destination", Variable.Type.address)]
        | _ -> [] )
        @ [("amount", Variable.Type.mutez)]
    | Booemrang | Do_nothing | Ping -> []
    | Update_variables _ | Generic_multisig_main | Vote | Call_operations_lambda
      ->
        assert false
end

module Entrypoint = struct
  type t = {name: string [@main]; control: Control.t; ability: Ability.t}
  [@@deriving sexp, fields, make]

  let collect_parameters {ability; _} = Ability.collect_parameters ability
end

module Smart_contract = struct
  type t =
    | Custom of
        { entrypoints: Entrypoint.t list
        ; fully_recognized: bool
        ; can_receive_funds: bool }
    | Generic_multisig
  [@@deriving sexp, variants]

  let make ?(fully_recognized = false) ?(can_receive_funds = false) entrypoints
      =
    Custom {entrypoints; fully_recognized; can_receive_funds}

  let collect_variables self =
    match self with
    | Generic_multisig ->
        let open Variable.Type in
        [("counter", nat); ("threshold", nat); ("keys", key_list)]
    | Custom {entrypoints; _} ->
        List.concat_map entrypoints ~f:(fun {Entrypoint.control; ability; _} ->
            Control.collect_variables control
            @ Ability.collect_variables ability )
        |> List.dedup_and_sort ~compare:Poly.compare
end

module Originated_contract = struct
  type t =
    { address: Address.t [@main]
    ; specification: Smart_contract.t
    ; variables: Variable.Record.t }
  [@@deriving sexp, fields, make]
end

module Key_pair = struct
  type backend = Ledger of {uri: string} | Unencrypted of {private_uri: string}
  [@@deriving sexp, variants]

  type t =
    { public_key_hash: Public_key_hash.t
    ; public_key: Public_key.t
    ; backend: backend [@main] }
  [@@deriving sexp, fields, make]
end

module Friend = struct
  type t = {public_key_hash: Public_key_hash.t; public_key: Public_key.t option}
  [@@deriving sexp, fields, make]
end

module Account = struct
  module Status = struct
    type t =
      | Originated_contract of Originated_contract.t
      | Key_pair of Key_pair.t
      | Friend of Friend.t
      | Contract_to_originate of {operation: string}
    [@@deriving sexp, variants]
  end

  type t =
    { id: string
    ; display_name: string
    ; balance: Z.t option
    ; state: Status.t [@main]
    ; history: Status.t list [@default []] }
  [@@deriving sexp, fields, make]

  let get_address {state; _} =
    let open Status in
    match state with
    | Originated_contract {address; _} -> Some address
    | Key_pair {public_key_hash; _} -> Some public_key_hash
    | Friend {public_key_hash; _} -> Some public_key_hash
    | Contract_to_originate _ -> None
end

module Operation = struct
  type id = string [@@deriving sexp]

  module Specification = struct
    type t =
      | Origination of
          { account: string
          ; specification: Smart_contract.t
          ; initialization: Variable.Map.t
          ; gas_wallet: string }
      | Transfer of
          { source: string
          ; destination: Address.t
          ; amount: Mutez.t
          ; entrypoint: string
          ; parameters: (string * Variable.Value.t) list }
    [@@deriving sexp, variants]
  end

  module Status = struct
    type tezos_operation =
      { operation_hash: string
      ; base_block_hash: string
      ; inclusion_block_hashes: string list }
    [@@deriving sexp, fields, make]

    type t =
      | Ordered
      | Work_in_progress of {op: tezos_operation option}
      | Success of {op: tezos_operation}
      | Failed of string
    [@@deriving sexp, variants]
  end

  type t = {id: id; order: Specification.t; status: Status.t; last_update: float}
  [@@deriving sexp, fields, make]
end

module Exchange_rates = struct
  type t = {timestamp: float; btc: float; eur: float; usd: float; eth: float}
  [@@deriving sexp, fields]
end
