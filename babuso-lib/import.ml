include Base

module Docpp = struct
  include Pp
  include Pp.O

  type no_tags = No_tags
  type untagged = no_tags t

  let ( +++ ) a b = a ++ space ++ b

  let verbatim_lines ?(on_line : string -> _ t = verbatim) lines : _ t =
    vbox (concat_map ~sep:cut ~f:on_line lines)

  let nbsp = verbatim " "
  let sexp (sexp : Sexp.t) = of_fmt Sexp.pp_hum sexp
  let sexpable to_sexp v = sexp (to_sexp v)

  (* let exn e = of_fmt Exn.pp e *)

  let itemize l = enumerate ~f:(box ~indent:2) l
  let parentheses c = text "(" ++ c ++ text ")"
  let comma = text "," ++ space

  let on_non_empty_list ?(empty = nop) ~f = function
    | [] -> empty
    | one :: more -> f one more

  let vbox_of_boxes = function
    | [] -> nop
    | one :: more ->
        vbox
          (List.fold ~init:(box one)
             ~f:(fun prev v -> prev ++ cut ++ box v)
             more )
end

module Exception = struct
  module Level = struct
    type t = Fatal | Bug | Runtime

    let to_string = function
      | Fatal -> "Fatal"
      | Bug -> "Bug"
      | Runtime -> "Runtime"
  end

  open Level

  let classify_exn = function
    | Out_of_memory | Stack_overflow -> Fatal
    | Match_failure _ | Assert_failure _ | Undefined_recursive_module _ -> Bug
    | _ -> Runtime

  let protect :
      ?finally:(unit -> unit) -> ?on_runtime:(exn -> 'a) -> (unit -> 'a) -> 'a =
   fun ?(finally = fun () -> ()) ?on_runtime f ->
    try
      let x = f () in
      finally () ; x
    with e -> (
      finally () ;
      match (classify_exn e, on_runtime) with
      | Runtime, Some f -> f e
      | _ -> raise e )

  let _all_to_pps : (exn -> Docpp.untagged option) list ref = ref []
  let register_pp f = _all_to_pps := f :: !_all_to_pps

  let () =
    register_pp
      Docpp.(
        function
        | Failure s -> Some (textf "Failure: %s" s)
        | Invalid_argument s -> Some (textf "Invalid-argument: %s" s)
        | Sys_error s -> Some (textf "Sys-error: %s" s)
        | Unix.Unix_error (err, a, b) ->
            Some
              (textf "Unix-error: %s (function: %S, argument: %S)"
                 (Unix.error_message err) a b )
        | _ -> None)

  let to_pp e =
    let open Docpp in
    filter_map_tags
      ~f:(fun _ -> None)
      (box
         ( textf "[%s-level]" (Level.to_string (classify_exn e))
         +++
         match List.find_map !_all_to_pps ~f:(fun f -> f e) with
         | Some s -> s
         | None -> verbatim (Exn.to_string e) ) )

  let is_runtime r =
    match classify_exn r with Runtime -> true | Fatal | Bug -> false

  let ignore_runtime ?formatter f =
    try f ()
    with e when is_runtime e -> (
      match formatter with
      | None -> ()
      | Some ppf ->
          Fmt.pf ppf "@[Exception ignored:@ %a@]@.%!" Pp.to_fmt (to_pp e) )
end

module Failure = struct
  exception F of {doc: Docpp.untagged; code: int}

  let raise ?(code = 2) doc = raise (F {doc; code})
  let raise_textf ?code fmt = Fmt.kstr (fun s -> raise ?code (Docpp.text s)) fmt

  let of_result :
      f:('error -> _ Docpp.t) -> ('ok, 'error) Result.t -> 'raised_out =
   fun ~f -> function Ok o -> o | Error e -> raise (f e)

  let of_none ~f = function None -> raise (f ()) | Some s -> s
  let assert_true cond ~f = if cond then () else raise (f ())

  let () =
    Stdlib.Printexc.register_printer (function
      | F {doc; _} -> Some (Fmt.str "%a" Pp.to_fmt doc)
      | _ -> None ) ;
    Exception.register_pp (function F {doc; _} -> Some doc | _ -> None)

  let run_or_die f =
    try f () with
    | F {doc; code} ->
        Fmt.epr "@[<2>ERROR:@ %a@]\n%!" Docpp.to_fmt doc ;
        Caml.exit code
    | Failure s ->
        Fmt.epr "EXCEPTION-FAILURE:@ %a@]\n%!" Fmt.text s ;
        Caml.exit 3
    | e ->
        Fmt.epr "@[<2>EXCEPTION:@ %a@]\n%!" Exn.pp e ;
        Caml.exit 4
end

module Json = struct
  type t = Ezjsonm.value

  let of_string : string -> t = Ezjsonm.value_from_string

  let of_lines : string list -> t =
   fun l -> of_string (String.concat ~sep:"\n" l)

  let to_string : t -> string = Ezjsonm.value_to_string ~minify:false
  let to_string_minify : t -> string = Ezjsonm.value_to_string ~minify:true

  let pp ppf (json : t) =
    let open Fmt in
    lines ppf (Ezjsonm.value_to_string ~minify:false json)

  let pp_short ppf (v : t) =
    let open Fmt in
    let str = to_string_minify v in
    match (String.length str > 30, str.[0]) with
    | false, _ -> pf ppf "%s" str
    | true, '{' -> pf ppf "%s...}" (String.sub str ~pos:0 ~len:32)
    | true, '[' -> pf ppf "%s...]" (String.sub str ~pos:0 ~len:32)
    | true, '"' -> pf ppf "%s...\"" (String.sub str ~pos:0 ~len:32)
    | true, _ -> pf ppf "%s..." (String.sub str ~pos:0 ~len:32)

  module Q = struct
    let fail ?json fmt = ignore json ; Fmt.failwith fmt

    let field ~k = function
      | `O l -> (
        try List.Assoc.find_exn l ~equal:String.equal k
        with _ -> fail ~json:(`O l) "Field %S not present." k )
      | other -> fail ~json:other "Field %S: not an object" k

    let list : t -> t list = Ezjsonm.get_list Fn.id
    let string_list : t -> string list = Ezjsonm.get_strings
    let string = Ezjsonm.get_string
    let int = Ezjsonm.get_int
    let float = Ezjsonm.get_float
    let string_field ~k j = field ~k j |> string
    let string_list_field ~k v = field ~k v |> string_list
    let int_field ~k j = field ~k j |> int
    let float_field ~k j = field ~k j |> float
  end

  module C = struct
    let obj l = Ezjsonm.dict l
    let string s = Ezjsonm.string s
    let bool = Ezjsonm.bool
    let null : t = `Null
    let a c : t = `A c
    let strings l = a (List.map ~f:string l)
    let int i = Ezjsonm.int i
    let float f = Ezjsonm.float f

    let ( @@@ ) : t -> t -> t =
     fun a b ->
      match (a, b) with
      | `O la, `O lb -> `O (la @ lb)
      | `A la, `A lb -> `A (la @ lb)
      | _ ->
          Fmt.failwith
            "Json-construction: invalid types (need both to be arrays or \
             objects)"

    (* ~attachments:
       [("json-left", Content.json a); ("json-right", Content.json b)] *)

    let ( --> ) x b = obj [(x, b)]
  end

  let%expect_test _ =
    let json = C.(("hello" --> bool false) @@@ ("world" --> a [int 3])) in
    Fmt.pr "%a" pp_short json ;
    [%expect {| {"hello":false,"world":[3]} |}] ;
    Fmt.pr "%a" pp_short Q.(field json ~k:"world") ;
    [%expect {| [3] |}] ;
    Exception.ignore_runtime ~formatter:Fmt.stdout (fun () ->
        Fmt.pr "%a" pp_short Q.(field json ~k:"noworld") ) ;
    [%expect
      {|
      Exception ignored: [Runtime-level] Failure: Field "noworld" not present.
      |}] ;
    ()
end
