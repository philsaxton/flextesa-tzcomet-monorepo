(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 TQ Tezos <contact@tqtezos.com>                         *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open! Base
open Import
open Tezai_contract_metadata

module Uri = struct
  module Fetcher = struct
    type gateway = {main: string; alternate: string}
    type t = {gateway: gateway}

    let create () =
      let main = "https://gateway.ipfs.io/ipfs/" in
      let alternate = "https://dweb.link/ipfs/" in
      {gateway= {main; alternate}}

    let fetcher = create ()
    let gateway = fetcher.gateway
  end

  let rec needs_context_address =
    let open Metadata_uri in
    function
    | Storage {address= None; _} -> true
    | Web _ | Storage _ | Ipfs _ -> false
    | Hash {target; _} -> needs_context_address target

  let to_ipfs_gateway ~alt_gateway ~cid ~path =
    let gateway =
      if alt_gateway then Fetcher.gateway.alternate else Fetcher.gateway.main
    in
    Fmt.str "%s%s%s" gateway cid path

  let fetch ?limit_bytes ?prefix (ctxt : 'a Context.t) uri ~current_contract =
    let log =
      match prefix with
      | None -> dbgf ctxt "Uri.fetch.log: %s"
      | Some prefix -> dbgf ctxt "%s: %s" prefix in
    let open Lwt.Infix in
    let logf fmt = Fmt.kstr (fun s -> log s) fmt in
    let not_implemented s = Fmt.failwith "Not Implemented: %s" s in
    dbgf ctxt "Fetching ============== " ;
    let rec resolve =
      let open Metadata_uri in
      function
      | Web http_uri ->
          logf "HTTP %S" http_uri ;
          ctxt#http_client.get ?limit_bytes http_uri
      | Ipfs {cid; path} ->
          logf "IPFS CID %S path %S" cid path ;
          let gatewayed = to_ipfs_gateway ~alt_gateway:false ~cid ~path in
          (* resolve (Web gatewayed) *)
          Lwt.catch
            (fun () -> resolve (Web gatewayed))
            (fun e ->
              dbgf ctxt "Trying alternate IPFS gateway due to exception: %s"
                (Exn.to_string e) ;
              let gatewayed_alt = to_ipfs_gateway ~alt_gateway:true ~cid ~path in
              resolve (Web gatewayed_alt) )
      | Storage {network= None; address; key} ->
          let addr =
            match address with
            | Some s -> s
            | None -> (
              match current_contract with
              | None -> Fmt.failwith "Missing current contract"
              | Some s -> s ) in
          logf "Using address %S (key = %S)" addr key ;
          Query_nodes.metadata_value ctxt ~address:addr ~key
      | Storage {network= Some network; address; key} ->
          logf "storage %s %a %S" network Fmt.Dump.(option string) address key ;
          Fmt.kstr not_implemented "storage uri with network = %s" network
      | Hash {kind= `Sha256; value; target} ->
          let expected =
            match Digestif.of_raw_string_opt Digestif.sha256 value with
            | Some s -> s
            | None ->
                Fmt.failwith "%a is not a valid SHA256 hash" Hex.pp
                  (Hex.of_string value) in
          logf "sha256: %a" (Digestif.pp Digestif.sha256) expected ;
          resolve target
          >>= fun content ->
          Lwt.return
            (Result.map
               ~f:(fun content ->
                 let obtained = Digestif.digest_string Digestif.sha256 content in
                 logf "hash of content: %a"
                   (Digestif.pp Digestif.sha256)
                   obtained ;
                 match
                   Digestif.unsafe_compare Digestif.sha256 expected obtained
                 with
                 | 0 -> content
                 | _ ->
                     Fmt.failwith
                       "Hash of content %a is different from expected %a"
                       (Digestif.pp Digestif.sha256)
                       obtained
                       (Digestif.pp Digestif.sha256)
                       expected )
               content ) in
    resolve uri
end

module Content = struct
  module Validation = struct
    module Error = struct
      type t =
        | Forbidden_michelson_instruction of {view: string; instruction: string}
        | Michelson_version_not_a_protocol_hash of {view: string; value: string}

      let pp ppf =
        let open Fmt in
        let textf f = kstr (fun s -> (box text) ppf s) f in
        function
        | Forbidden_michelson_instruction {view; instruction} ->
            textf "Forbidden Michelson instruction %S in view %S" instruction
              view
        | Michelson_version_not_a_protocol_hash {view; value} ->
            textf "Michelson version %S in view %S is not a protocol hash" value
              view
    end

    module Warning = struct
      type t =
        | Wrong_author_format of string
        | Unexpected_whitespace of {field: string; value: string}
        | Self_unaddressed of {view: string; instruction: string option}

      let pp ppf =
        let open Fmt in
        let textf f = kstr (fun s -> (box text) ppf s) f in
        function
        | Wrong_author_format auth ->
            textf "Wrong format for author field: %S" auth
        | Unexpected_whitespace {field; value} ->
            textf "Unexpected whitespace character(s) in field %S = %S" field
              value
        | Self_unaddressed {view; instruction} ->
            textf "SELF instruction not followed by ADDRESS (%s) in view %S"
              (Option.value instruction ~default:"by nothing")
              view
    end

    module Data = struct
      let author_re = lazy Re.Posix.(re "^[^\\<\\>]*<[^ ]+>$" |> compile)

      let forbidden_michelson_instructions =
        [ "AMOUNT"
        ; "CREATE_CONTRACT"
        ; "SENDER"
        ; "SET_DELEGATE"
        ; "SOURCE"
        ; "TRANSFER_TOKENS" ]
    end

    open Tezai_contract_metadata.Metadata_contents
    open Data

    let validate ?(protocol_hash_is_valid = fun _ -> true) (metadata : t) =
      let errors = ref [] in
      let warnings = ref [] in
      let error e = errors := e :: !errors in
      let warning e = warnings := e :: !warnings in
      let nl_or_tab = function '\n' | '\t' -> true | _ -> false in
      let nl_or_tab_or_sp = function '\n' | '\t' | ' ' -> true | _ -> false in
      let check_for_whitespace ?(whitespace = nl_or_tab) field value =
        if Base.String.exists value ~f:whitespace then
          warning Warning.(Unexpected_whitespace {field; value}) in
      let check_author = function
        | s when not (Re.execp (Lazy.force author_re) s) ->
            warning Warning.(Wrong_author_format s)
        | _ -> () in
      List.iter
        ~f:(fun a ->
          check_author a ;
          check_for_whitespace "author" a )
        metadata.authors ;
      Option.iter ~f:(check_for_whitespace "name") metadata.name ;
      Option.iter ~f:(check_for_whitespace "version") metadata.version ;
      let check_view (v : View.t) =
        let implementation (i : View.Implementation.t) =
          let open View.Implementation in
          match i with
          | Michelson_storage {code= Michelson_blob mich_code; version; _} -> (
              Option.iter
                ~f:(fun value ->
                  if protocol_hash_is_valid value then ()
                  else
                    error
                      (Error.Michelson_version_not_a_protocol_hash
                         {view= v.name; value} ) )
                version ;
              let open Tezos_micheline.Micheline in
              let node = root mich_code in
              let rec iter = function
                | Int _ | String _ | Bytes _ -> `Other "literal"
                | Prim (_loc, p, args, _annots) -> (
                    if
                      List.mem forbidden_michelson_instructions p
                        ~equal:String.equal
                    then
                      error
                        (Error.Forbidden_michelson_instruction
                           {view= v.name; instruction= p} ) ;
                    let _ = List.map ~f:iter args in
                    match p with
                    | "SELF" -> `Self
                    | "ADDRESS" -> `Address
                    | _ -> `Other p )
                | Seq (_loc, l) ->
                    let selves = List.map ~f:iter l in
                    List.fold
                      (selves : [`Address | `Other of string | `Self] list)
                      ~init:
                        (`Other "none" : [`Address | `Other of string | `Self])
                      ~f:(fun prev cur ->
                        match (prev, cur) with
                        | `Other _, _ -> cur
                        | `Self, `Address -> cur
                        | `Self, _ ->
                            warning
                              Warning.(
                                Self_unaddressed
                                  { view= v.name
                                  ; instruction=
                                      ( match cur with
                                      | `Self -> Some "SELF"
                                      | `Other p -> Some p
                                      | `Address -> assert false ) }) ;
                            cur
                        | `Address, _ -> cur ) in
              match iter node with
              | `Self ->
                  warning
                    Warning.(Self_unaddressed {view= v.name; instruction= None})
              | _ -> () )
          | Rest_api_query _ -> () in
        check_for_whitespace "view.name" v.name ~whitespace:nl_or_tab_or_sp ;
        List.iter ~f:implementation v.implementations in
      List.iter ~f:check_view metadata.views ;
      (List.rev !errors, List.rev !warnings)

    let pp ppf =
      let open Fmt in
      function
      | [], [] -> pf ppf "No errors nor warnings."
      | errs, warns ->
          let pp_events prompt pp =
            let itemize ppf = function
              | [] -> const string "None" ppf ()
              | more ->
                  (cut ++ list ~sep:cut (const string "* " ++ pp)) ppf more
            in
            vbox ~indent:2 (const string prompt ++ itemize) in
          vbox
            ( const (pp_events "Errors: " Error.pp) errs
            ++ cut
            ++ const (pp_events "Warnings: " Warning.pp) warns )
            ppf ()
  end

  let of_json s =
    try
      let add_if_not_present ~list ~present ~new_value =
        match List.exists !list ~f:present with
        | true -> ()
        | false -> list := new_value :: !list in
      let warnings = ref [] in
      let jsonm =
        let j = Ezjsonm.value_from_string s in
        let rec fix = function
          | (`String _ | `Float _ | `Bool _ | `Null) as v -> v
          | `A l -> `A (List.map l ~f:fix)
          | `O kv_list ->
              let f (k, v) =
                let fix_warn key replacement =
                  add_if_not_present ~list:warnings
                    ~present:(function
                      | `Fixed_legacy (a, _) -> String.equal a key )
                    ~new_value:(`Fixed_legacy (key, replacement)) ;
                  (replacement, fix v) in
                match k with
                | "michelson-storage-view" -> fix_warn k "michelsonStorageView"
                | "return-type" -> fix_warn k "returnType"
                | other -> (other, fix v) in
              `O (List.map kv_list ~f) in
        fix j in
      let contents = Json_encoding.destruct Metadata_contents.encoding jsonm in
      Ok (!warnings, contents)
    with e -> Tezos_error_monad.Error_monad.error_exn e
end
