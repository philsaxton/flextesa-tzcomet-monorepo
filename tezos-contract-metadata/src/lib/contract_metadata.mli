(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 TQ Tezos <contact@tqtezos.com>                         *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
open! Base
open! Import

module Uri : sig
  module Fetcher : sig
    type gateway = {main: string; alternate: string}
    type t = {gateway: gateway}

    val create : unit -> t
  end

  val fetch :
       ?limit_bytes:int
    -> ?prefix:string
    -> < nodes: Query_nodes.Node.t list ; .. > Context.t
    -> Tezai_contract_metadata.Metadata_uri.t
    -> current_contract:string option
    -> Http_client.result Lwt.t

  val needs_context_address : Tezai_contract_metadata.Metadata_uri.t -> bool
end

module Content : sig
  val of_json :
       string
    -> ( [`Fixed_legacy of string * string] list
         * Tezai_contract_metadata.Metadata_contents.t
       , Tezos_error_monad.Error_monad.tztrace )
       Result.t
  (** Create a contract from json. Return the contract and, if there are any
      legacy (kebab-case instead of CamelCase) keys, a list of warnings about
      those keys. The legacy keys are from an earlier version of TZIP-016 and
      should not be seen very often. *)

  module Validation : sig
    module Error : sig
      type t =
        | Forbidden_michelson_instruction of {view: string; instruction: string}
        | Michelson_version_not_a_protocol_hash of {view: string; value: string}

      val pp : Caml.Format.formatter -> t -> unit
    end

    module Warning : sig
      type t =
        | Wrong_author_format of string
        | Unexpected_whitespace of {field: string; value: string}
        | Self_unaddressed of {view: string; instruction: string option}

      val pp : Caml.Format.formatter -> t -> unit
    end

    module Data : sig
      val author_re : Re.re lazy_t
      val forbidden_michelson_instructions : string list
    end

    val validate :
         ?protocol_hash_is_valid:(string -> bool)
      -> Tezai_contract_metadata.Metadata_contents.t
      -> Error.t list * Warning.t list
    (** Run the validation on a metadata instance. The default
        [protocol_hash_is_valid] is [(fun _ -> true)], so by default the error
        [Michelson_version_not_a_protocol_hash _] is not reported (for library
        dependency reasons). *)

    val pp : Caml.Format.formatter -> Error.t list * Warning.t list -> unit
  end
end
