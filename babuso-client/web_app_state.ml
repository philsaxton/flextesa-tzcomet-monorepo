open Import
open Babuso_lib
open Wallet_data
open Lwd_bootstrap

type t =
  { accounts: Account.t list Reactive.var
  ; operations: Operation.t list Reactive.var
  ; exchange_rates: Exchange_rates.t option Reactive.var
  ; failures: string list Reactive.var
  ; alerts: Protocol.Message.Down.Alert.t list Reactive.var
  ; low_resolution_date_10s: float Reactive.var
  ; low_resolution_date_5m: float Reactive.var }

let make () =
  { accounts= Reactive.var []
  ; operations= Reactive.var []
  ; failures= Reactive.var []
  ; exchange_rates= Reactive.var None
  ; alerts= Reactive.var []
  ; low_resolution_date_10s= Reactive.var (Unix.gettimeofday ())
  ; low_resolution_date_5m= Reactive.var (Unix.gettimeofday ()) }

let get ctxt : t = ctxt#state
let accounts ctxt = (get ctxt).accounts
let operations ctxt = (get ctxt).operations
let failures ctxt = (get ctxt).failures
let exchange_rates ctxt = (get ctxt).exchange_rates
let get_alerts ctxt = (get ctxt).alerts |> Reactive.get

let get_low_resolution_date ctxt = function
  | `Seconds_10 -> (get ctxt).low_resolution_date_10s |> Reactive.get
  | `Minutes_5 -> (get ctxt).low_resolution_date_5m |> Reactive.get

let add_failure ctxt s =
  let cur = (get ctxt).failures in
  Reactive.set cur (s :: Reactive.peek cur)

let get_all_the_things ctxt =
  let open Protocol.Message in
  let open Lwt.Infix in
  Server_communication.call Up.All_accounts
  >>= (function
        | Down.Account_list l ->
            Reactive.set (accounts ctxt) l ;
            Lwt.return_unit
        | other ->
            Fmt.kstr (add_failure ctxt) "Wrong return: %a" Sexp.pp_hum
              (Down.sexp_of_t other) ;
            Lwt.return_unit )
  >>= fun () ->
  Server_communication.call Up.all_operations
  >>= function
  | Down.Operation_list l ->
      Reactive.set (operations ctxt) l ;
      Lwt.return_unit
  | other ->
      Fmt.kstr (add_failure ctxt) "Wrong return: %a" Sexp.pp_hum
        (Down.sexp_of_t other) ;
      Lwt.return_unit

let init ctxt =
  let open Lwt.Infix in
  let open Protocol.Message in
  let backoff = ref 1. in
  let events_since = ref Float.min_value in
  let record_failuref fmt =
    Fmt.kstr
      (fun s ->
        (add_failure ctxt) (Fmt.str "Event loop error: %s" s) ;
        backoff := !backoff *. 2. ;
        Js_of_ocaml_lwt.Lwt_js.sleep !backoff )
      fmt in
  let rec loop () =
    Lwt.catch
      (fun () ->
        dbgf "Getting events" ;
        Server_communication.call ~timeout:10_000. (Up.Get_events !events_since)
        >>= function
        | Down.Events (events, date) ->
            events_since := date ;
            dbgf "Received events: %f %a" date
              Fmt.(Dump.list Sexp.pp)
              (List.map ~f:Down.sexp_of_event events) ;
            backoff := 1. ;
            let reloaded_all_once = ref false in
            Lwt_list.iter_s
              (function
                | Down.Please_just_reload_all reason ->
                    dbgf "Asked to reload all: %s -> %s" reason
                      (if !reloaded_all_once then "NO" else "OK") ;
                    if !reloaded_all_once then Lwt.return_unit
                    else
                      get_all_the_things ctxt
                      >>= fun () ->
                      reloaded_all_once := true ;
                      Lwt.return_unit
                | Down.New_exchange_rates rates ->
                    Reactive.set (get ctxt).exchange_rates (Some rates) ;
                    Lwt.return_unit
                | Down.Alert_on alert ->
                    let v = (get ctxt).alerts in
                    Reactive.set v (alert :: Reactive.peek v) ;
                    Lwt.return_unit
                | Down.Alert_off {id= alert_id} ->
                    let v = (get ctxt).alerts in
                    Reactive.set v
                      (List.filter
                         ~f:String.(fun al -> al.id <> alert_id)
                         (Reactive.peek v) ) ;
                    Lwt.return_unit )
              events
            >>= fun () -> loop ()
        | other ->
            record_failuref "wrong server message %a" Sexp.pp
              (Down.sexp_of_t other)
            >>= fun () -> loop () )
      (fun exn ->
        record_failuref "comminucation error %a" Exn.pp exn
        >>= fun () -> loop () ) in
  let rec quick_loop () =
    let now = Unix.gettimeofday () in
    Reactive.set (get ctxt).low_resolution_date_10s now ;
    if Float.(Reactive.peek (get ctxt).low_resolution_date_5m + (60. * 5.) > now)
    then Reactive.set (get ctxt).low_resolution_date_5m now ;
    Js_of_ocaml_lwt.Lwt_js.sleep 10. >>= fun () -> quick_loop () in
  Lwt.async quick_loop ;
  Lwt.async (fun () ->
      begin
        Server_communication.call Up.get_exchange_rates
        >>= (function
              | Down.Exchange_rates (Some er) ->
                  Reactive.set (get ctxt).exchange_rates (Some er) ;
                  Lwt.return_unit
              | _ -> Lwt.return_unit )
        >>= fun () -> get_all_the_things ctxt >>= fun () -> loop ()
      end )
