open! Import
open Babuso_lib

let with_timeout timeout ~f =
  let open Lwt.Infix in
  Lwt.pick
    [ f ()
    ; ( Js_of_ocaml_lwt.Lwt_js.sleep timeout
      >>= fun () -> Fmt.kstr Lwt.fail_with "Timeout: %f" timeout ) ]

let counter = ref 0

let call ?(timeout = 10.) message =
  let body = Protocol.Message.Up.sexp_of_t message |> Sexp.to_string_mach in
  let open Lwt.Infix in
  let open Js_of_ocaml_lwt in
  with_timeout timeout
    ~f:
      XmlHttpRequest.(
        fun () ->
          Int.incr counter ;
          perform_raw_url ~contents:(`String body)
            (Fmt.str "ui-protocol?c=%d"
               !counter (* This seems to be a hack against caches? *) )
            ~override_method:`POST
          >>= fun frame ->
          dbgf "query#%d %s -> code: %d, content: %s" !counter body frame.code
            frame.content ;
          match frame.code with
          | 200 ->
              Lwt.return
                (Protocol.Message.Down.t_of_sexp
                   (Sexplib.Sexp.of_string frame.content) )
          | other ->
              Fmt.kstr Lwt.fail_with "Did not 200: %d: %S" other frame.content)

let call_with_units message ~wip ~success ~failure get_result =
  let open Protocol.Message in
  let open Lwt.Infix in
  wip () ;
  Lwt.async (fun () ->
      Lwt.catch
        (fun () ->
          call message
          >>= fun down ->
          match get_result down with
          | v -> success v ; Lwt.return_unit
          | exception e -> Lwt.fail e )
        (fun exn ->
          failure
            (Fmt.str "Call %a failed: %a" Sexp.pp_hum (Up.sexp_of_t message)
               Exn.pp exn ) ;
          Lwt.return_unit ) )

let call_with_var var message get_result =
  let open Lwd_bootstrap in
  let open Wip in
  call_with_units message get_result
    ~wip:(fun () -> Reactive.set var WIP)
    ~success:(fun v -> Reactive.set var (Success v))
    ~failure:(fun s -> Reactive.set var (Failure s))

let list_connected_ledgers var =
  let open Protocol.Message in
  call_with_var var Up.List_connected_ledgers (function
    | Down.Ledgers sl -> sl
    | _ -> Fmt.failwith "Wrong Response" )

(* let all_accounts var ~on_failure =
   let open Protocol.Message in
   call_with_units Up.All_accounts
     ~wip:(fun () -> ())
     ~failure:on_failure
     ~success:Lwd_bootstrap.(fun v -> Reactive.set var v)
     (function
       | Down.Account_list sl -> sl | _ -> Fmt.failwith "Wrong Response" ) *)
let call_with_var_unit var msg =
  let open Protocol.Message in
  call_with_var var msg (function
    | Down.Done -> ()
    | _ -> Fmt.failwith "Wrong Response" )

let import_account var name kind =
  let open Protocol.Message in
  call_with_var_unit var Up.(Import_account {name; kind})

let delete_account var ~id =
  let open Protocol.Message in
  call_with_var_unit var Up.(Delete_account id)

let get_configuration var =
  let open Protocol.Message in
  call_with_var var Up.Get_configuration (function
    | Down.Blob sl -> sl
    | _ -> Fmt.failwith "Wrong Response" )
