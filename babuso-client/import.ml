include Babuso_lib.Import

let dbg fmt = Fmt.pf Fmt.stdout "@[debug: %a@]%!" fmt ()
let dbgf fmt = Fmt.(kstr (fun s -> dbg (const string s))) fmt

module Context = struct type 'a t = 'a constraint 'a = < .. > end

module Browser_window = struct
  module Reactive = Lwd_bootstrap.Reactive

  type width_state = [`Thin | `Wide]
  type t = {width: width_state option Reactive.var}

  open Js_of_ocaml

  let create ?(threshold = 992) () =
    let find_out () =
      match Js.Optdef.to_option Dom_html.window##.innerWidth with
      | Some s when s >= threshold -> Some `Wide
      | Some _ -> Some `Thin
      | None -> None in
    let width = Reactive.var (find_out ()) in
    Dom_html.window##.onresize :=
      Dom_html.handler (fun _ ->
          let current = Reactive.peek width in
          let new_one = find_out () in
          if Poly.(current <> new_one) then Reactive.set width new_one ;
          Js._true ) ;
    {width}

  let get (c : < window: t ; .. > Context.t) = c#window
  let width c = (get c).width |> Reactive.get
end

module Wip = struct
  type 'a t = Idle | Success of 'a | Failure of string | WIP
  [@@deriving sexp, variants]
end
