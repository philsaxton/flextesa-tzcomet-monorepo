# Flextesa TZComet Monorepo

## Commands

    eval $(./plaese.sh env)
    ./please.sh build
    ./please.sh test
    ./please.sh lint

## Subtrees

### Thu, 03 Mar 2022 15:51:00 -0500

Inspired by experiments at
[smondet/gen-smartml](https://github.com/smondet/gen-smartml).

```sh
spytmp=$HOME/tmp/smartpy-public
git clone https://gitlab.com/SmartPy/smartpy.git "$spytmp" # Or
git -C "$spytmp" pull origin master
rm -fr smartml-extract
mkdir -p smartml-extract
cp -r $spytmp/smartML/core smartml-extract/smartml-core
cp -r $spytmp/smartML/utils_pure smartml-extract/smartml-utils-pure
cp -r $spytmp/smartML/michelson_base smartml-extract/smartml-michelson-base
cp -r $spytmp/smartML/tools smartml-extract/smartml-tools
cp -r $spytmp/smartML/michel smartml-extract/smartml-michel
cp -r $spytmp/smartML/utils smartml-extract/smartml-utils
#sed -i 's/(public_name/;; (public_name/g' smartml-extract/*/dune
sed -i 's/9@27-30@32-40@8/9-27-30-32-40-8/' smartml-extract/*/dune
sed -i 's/yojson)/num yojson)/' smartml-extract/smartml-utils-pure/dune
for d in $(find smartml-extract/ -type d ) ; do echo '(lang dune 2.9)' > "$d/dune-project" ; done
```



### Fri, 21 Jan 2022 12:36:24 -0500

After pushing to
[tezos/flextesa!54](https://gitlab.com/tezos/flextesa/-/merge_requests/54)
(*tezai-base58-digest updates*, **@smondet**):

```sh
git subtree pull --prefix flextesa \
    https://gitlab.com/tezos/flextesa master --squash
```



### Wed, 29 Dec 2021 10:38:00 -0500

```sh
git subtree add --prefix flextesa \
    https://gitlab.com/tezos/flextesa master --squash
git subtree add --prefix tzcomet \
    https://github.com/oxheadalpha/TZComet master --squash
git subtree add --prefix tezos-contract-metadata \
    https://github.com/oxheadalpha/tezos-contract-metadata main --squash
git subtree add --prefix let-def-lwd \
    https://github.com/let-def/lwd.git master --squash
```
