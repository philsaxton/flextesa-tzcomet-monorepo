open! Base

let default_dot_ocamlformat =
  {|version=0.19.0
profile=compact
break-collection-expressions=fit-or-vertical
exp-grouping=preserve
parse-docstrings
|}

module Path = struct
  include Caml.Filename

  let ( // ) = concat
end

module Dune_project = struct
  type t = {kind: [`Library]; libraries: string list}

  let apply proj ~path ~name =
    Stdio.Out_channel.write_all
      Path.(path // "dune-project")
      ~data:"(lang dune 2.9)\n" ;
    let dune_file =
      Fmt.str
        {|
(library
 (name %s)
 (public_name %s)
 (inline_tests)
 (preprocess
  (pps ppx_inline_test ppx_expect))
 (libraries %s))
|}
        (String.map name ~f:(function '-' -> '_' | c -> c))
        name
        (String.concat ~sep:" " proj.libraries) in
    Stdio.Out_channel.write_all Path.(path // "dune") ~data:dune_file ;
    Stdio.Out_channel.write_all Path.(path // Fmt.str "%s.opam" name) ~data:"" ;
    ()
end

module Project = struct
  type t =
    { name: string
    ; path: string
    ; ocamlformat: [`Default | `No]
    ; dune_project: Dune_project.t option }

  let make ?dune_project ?(ocamlformat = `Default) ?path name =
    let path = Option.value path ~default:name in
    {name; path; ocamlformat; dune_project}

  let apply p =
    begin
      match p.ocamlformat with
      | `No -> ()
      | `Default ->
          Stdio.Out_channel.write_all
            Path.(p.path // ".ocamlformat")
            ~data:default_dot_ocamlformat
    end ;
    Option.iter p.dune_project ~f:(Dune_project.apply ~path:p.path ~name:p.name) ;
    ()
end

module All = struct
  let _all : Project.t list ref = ref []

  let add ?ocamlformat ?path ?dune_library_deps name =
    let dune_project =
      match dune_library_deps with
      | None -> None
      | Some l -> Some Dune_project.{kind= `Library; libraries= l} in
    _all := Project.make ?ocamlformat name ?path ?dune_project :: !_all

  let iter f = List.iter !_all ~f
end

let () =
  All.add "tezai-contract-metadata" ~dune_library_deps:["tezos-micheline"; "fmt"] ;
  All.add "tezai-michelson" ~dune_library_deps:["tezos-micheline"; "fmt"] ;
  All.add "autifest"

let () =
  match Caml.Sys.argv.(1) with
  | "apply" ->
      All.iter Project.apply ;
      Fmt.epr "Done, don't forget to `dune build @fmt`\n%!"
  | (exception _) | _ ->
      Fmt.epr "usage: autifest.exe {apply}!\n%!" ;
      Caml.exit 2
