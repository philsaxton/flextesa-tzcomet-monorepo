open Import

module Raw = struct
  let command_to_string_list cmd =
    let open Caml in
    let i = Unix.open_process_in cmd in
    let rec loop acc = try loop (input_line i :: acc) with _ -> List.rev acc in
    let res = loop [] in
    let status = Unix.close_process_in i in
    match status with
    | Unix.WEXITED 0 -> res
    | _ -> Fmt.failwith "Command %S returned non-zero" cmd
end

module Shell = struct
  type t = Default

  let default () : t = Default
  let with_fake_capability (_ctxt : < shell: t ; .. >) f = f

  let command_to_string_list ctxt =
    with_fake_capability ctxt (fun s -> Raw.command_to_string_list s)

  let exec_to_string_list ctxt l =
    command_to_string_list ctxt
      (String.concat ~sep:" " (List.map l ~f:Path.quote))
end

module File = struct
  type file_io = File_io

  let default () = File_io
  let with_fake_capability (_ctxt : < file_io: file_io ; .. >) f = f
  let file_exists ctxt = with_fake_capability ctxt Caml.Sys.file_exists

  let read_lines ctxt =
    with_fake_capability ctxt (fun p ->
        let open Caml in
        let o = open_in p in
        let r = ref [] in
        try
          while true do
            r := input_line o :: !r
          done ;
          assert false
        with _ -> close_in o ; List.rev !r )

  let write_lines ctxt =
    with_fake_capability ctxt (fun p l ->
        let o = Caml.open_out p in
        List.iter l ~f:(Caml.Printf.fprintf o "%s\n") ;
        Caml.close_out o )

  let make_path ctxt =
    with_fake_capability ctxt (fun p ->
        let (_ : string list) =
          Fmt.kstr Raw.command_to_string_list "mkdir -p %s" (Path.quote p) in
        () )
end

module Clock = struct
  type t = Clock

  let default () = Clock
  let with_fake_capability (_ctxt : < clock: t ; .. >) f = f
  let sleep ctxt = with_fake_capability ctxt (fun f -> Unix.sleepf f)
end
