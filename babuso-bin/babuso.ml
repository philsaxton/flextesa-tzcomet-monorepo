open Import
module Debug_app = Debug (struct let name = "babuso" end)
open Debug_app.O
open Babuso_lib
open Wallet_data
module Fresh_id = struct let make () = Uuidm.(v `V4 |> to_string) end

module Json_web_api = struct
  let call ?(meth = `Get) url =
    let headers = [("Content-type", "application/json")] in
    let treat_response = function
      | Ok Ezcurl_core.{code= 200; body; _} ->
          dbgf "API success: %s -> %s"
            ( match meth with
            | `Get -> "GET"
            | `Post s -> Ezjsonm.value_to_string s )
            url ;
          Ezjsonm.value_from_string body
      | Ok Ezcurl_core.{code; body; _} ->
          Failure.raise_textf "API %s returned %d %S" url code body
      | Error (code, msg) ->
          Failure.raise_textf "API %s failed %s %S" url (Curl.strerror code) msg
    in
    match meth with
    | `Get -> Ezcurl.get ~headers ~url () |> treat_response
    | `Post json -> (
        let body = Ezjsonm.value_to_string json in
        let as_file =
          let f = Caml.Filename.temp_file "curl-" ".data" in
          let o = Caml.open_out f in
          Caml.output_string o body ; Caml.close_out o ; f in
        dbgf "post: %S %s" body as_file ;
        (* WUUUTTTT!
           Ezcurl.post ~headers (* ~content:(`String body) *) ~url
             ~params:
               (* [ Curl.CURLFORM_CONTENT
                   ("body", body, Curl.CONTENTTYPE "application/json") ] *)
               [Curl.CURLFORM_FILECONTENT ("body", as_file, Curl.DEFAULT)]
             ()
           |> treat_response *)
        try
          let res =
            Fmt.kstr Io.Raw.command_to_string_list
              "curl --silent --show-error -H 'Content-type: application/json' \
               --data %s %s"
              (Path.quote body) (Path.quote url) in
          String.concat ~sep:"\n" res |> Ezjsonm.value_from_string
        with e -> Failure.raise_textf "POST-API %S failed: %a" url Exn.pp e )
end

module Octez_node = struct
  type t = {base_url: string} [@@deriving sexp]
  type set = t list [@@deriving sexp]

  let rpc t ?(meth = `Get) path =
    let url = Path.concat t.base_url path in
    let headers = [("Content-type", "application/json")] in
    let treat_response = function
      | Ok Ezcurl_core.{code= 200; body; _} ->
          dbgf "RPC success %s: %s -> %s" t.base_url
            ( match meth with
            | `Get -> "GET"
            | `Post s -> Ezjsonm.value_to_string s )
            path ;
          Ezjsonm.value_from_string body
      | Ok Ezcurl_core.{code; body; _} ->
          Failure.raise_textf "RPC %s returned %d %S" path code body
      | Error (code, msg) ->
          Failure.raise_textf "RPC %s failed %s %S" path (Curl.strerror code)
            msg in
    match meth with
    | `Get -> Ezcurl.get ~headers ~url () |> treat_response
    | `Post json -> (
        let body = Ezjsonm.value_to_string json in
        let as_file =
          let f = Caml.Filename.temp_file "curl-" ".data" in
          let o = Caml.open_out f in
          Caml.output_string o body ; Caml.close_out o ; f in
        dbgf "post: %S %s" body as_file ;
        (* WUUUTTTT!
           Ezcurl.post ~headers (* ~content:(`String body) *) ~url
             ~params:
               (* [ Curl.CURLFORM_CONTENT
                   ("body", body, Curl.CONTENTTYPE "application/json") ] *)
               [Curl.CURLFORM_FILECONTENT ("body", as_file, Curl.DEFAULT)]
             ()
           |> treat_response *)
        try
          let res =
            Fmt.kstr Io.Raw.command_to_string_list
              "curl --silent --show-error -H 'Content-type: application/json' \
               --data %s %s"
              (Path.quote body) (Path.quote url) in
          String.concat ~sep:"\n" res |> Ezjsonm.value_from_string
        with e -> Failure.raise_textf "POST-RPC %S failed: %a" path Exn.pp e )
end

module Octez_client = struct
  module Flextesa_key_pairs = struct
    module Crypto = Tezai_tz1_crypto.Signer

    let sk seed = Crypto.Secret_key.of_seed seed
    let pk seed = sk seed |> Crypto.Public_key.of_secret_key
    let pkh seed = pk seed |> Crypto.Public_key_hash.of_public_key
    let pubkey n = pk n |> Crypto.Public_key.to_base58
    let pubkey_hash n = pkh n |> Crypto.Public_key_hash.to_base58
    let private_key n = "unencrypted:" ^ Crypto.Secret_key.to_base58 (sk n)
  end

  module Fake = struct
    let fake_animals = "crouching-tiger-hidden-dragon"

    let fake_ledger uri =
      object
        method uri = uri
        method pkh = Flextesa_key_pairs.pubkey_hash uri
        method pk = Flextesa_key_pairs.pubkey uri
        method sk = Flextesa_key_pairs.private_key uri
      end

    let slow_accounts : string list ref = ref []
    (* This is not saved to disk; may not be in sync' with what the
       octez client knows. *)

    let command ctxt ~real_one args =
      match args with
      | ["list"; "connected"; "ledgers"] ->
          Io.Clock.sleep ctxt 2. ;
          [ Fmt.str "## Ledger `%s`" fake_animals
          ; "Found a Tezos Wallet 4.2.42 (git-description: "
          ; "more garbage" ]
      | "import" :: "secret" :: "key" :: "fail" :: _ ->
          Io.Clock.sleep ctxt 1. ;
          Fmt.failwith "Fake failure!"
      | "import" :: "secret" :: "key" :: name :: uri :: _
        when String.is_prefix ~prefix:"ledger://" uri ->
          Io.Clock.sleep ctxt 1. ;
          let ledger = fake_ledger uri in
          slow_accounts := name :: !slow_accounts ;
          (* accounts := `Ledger (name, ledger) :: !accounts ; *)
          real_one ["import"; "secret"; "key"; name; ledger#sk; "--force"]
      | "originate"
        :: "contract" :: _ :: "transferring" :: _ :: "from" :: name :: _
        when List.mem !slow_accounts name ~equal:String.equal ->
          dbgf "Sloooowwwing down %a" Fmt.Dump.(list string) args ;
          Io.Clock.sleep ctxt 3. ;
          real_one args
      | other -> real_one other
  end

  type t =
    {executable: string; data_dir: string; node: Octez_node.t; faking_it: bool}
  [@@deriving sexp]

  let get ctxt : t = ctxt#octez_client

  let command ctxt self args =
    let real_one args =
      Io.Shell.command_to_string_list ctxt
        (String.concat ~sep:" "
           (List.map ~f:Path.quote
              ([self.executable; "--base-dir"; self.data_dir] @ args) ) ) in
    if self.faking_it then Fake.command ctxt args ~real_one else real_one args

  let command ctxt args =
    dbgf "octez-client %a\n  command: %a" Sexp.pp
      (sexp_of_t (get ctxt))
      Fmt.Dump.(list string)
      args ;
    command ctxt (get ctxt) args

  let ensure_initialized ctxt =
    let client = get ctxt in
    Io.File.make_path ctxt client.data_dir ;
    let (_ : string list) =
      command ctxt
        ["--endpoint"; client.node.Octez_node.base_url; "config"; "update"]
    in
    ()
end

module Events = struct
  module Ev = Protocol.Message.Down

  type event = {timestamp: float; content: Ev.event}
  type t = {mutable all: event list; mutex: Mutex.t; condition: Condition.t}

  let create () =
    {all= []; mutex= Mutex.create (); condition= Condition.create ()}

  let get ctxt : t = ctxt#events

  let add ctxt events =
    let self = get ctxt in
    Mutex.lock self.mutex ;
    let timestamp = Unix.gettimeofday () in
    List.iter events ~f:(fun content ->
        self.all <- {timestamp; content} :: self.all ) ;
    Condition.broadcast self.condition ;
    Mutex.unlock self.mutex ;
    ()

  let get ctxt ~since =
    let self = get ctxt in
    Mutex.lock self.mutex ;
    let interesting_event since ev = Float.(ev.timestamp - since > 0.001) in
    let there_are_events () =
      match self.all with
      | ev :: _ when interesting_event since ev -> true
      | _ -> false in
    while not (there_are_events ()) do
      Condition.wait self.condition self.mutex
    done ;
    let to_return = List.take_while self.all ~f:(interesting_event since) in
    let to_keep =
      let since = Unix.gettimeofday () -. 300. in
      List.take_while self.all ~f:(interesting_event since) in
    dbgf "Events GC: keeping %d events" (List.length to_keep) ;
    self.all <- to_keep ;
    (* let all = self.all in
       self.all <- [] ; *)
    Mutex.unlock self.mutex ;
    to_return

  let add_generic ctxt fmt =
    Fmt.kstr (fun s -> add ctxt [Ev.Please_just_reload_all s]) fmt

  let account_added ctxt (acc : Account.t) =
    add_generic ctxt "account_added %s" acc.id

  let account_removed ctxt (id : string) =
    add_generic ctxt "account_removed %s" id

  let account_changed ctxt (id : string) =
    add_generic ctxt "account_changed %s" id

  let operation_added ctxt (acc : Operation.t) =
    add_generic ctxt "operation_added %s" acc.id

  let operation_removed ctxt (id : string) =
    add_generic ctxt "operation_removed %s" id

  let operation_changed ctxt (id : string) =
    add_generic ctxt "operation_changed %s" id

  let new_exchange_rates ctxt rates = add ctxt [Ev.New_exchange_rates rates]

  let add_alert ctxt content =
    let id = Fresh_id.make () in
    add ctxt [Ev.alert_on (Ev.Alert.make ~id ~content)] ;
    id

  let remove_alert ctxt id = add ctxt [Ev.alert_off ~id]
end

module Theme = struct
  type t = [`Lumen | `Cerulean | `Greyscale | `Solar]
  [@@deriving sexp, variants]

  let css self : string =
    let extra_css_light = {css|
code { color: #333; }
|css} in
    let extra_css_dark = {css|
code { color: #888; }
|css} in
    Variants.map self
      ~lumen:(fun _ -> Data.lumen_css ^ extra_css_light)
      ~cerulean:(fun _ -> Data.cerulean_css ^ extra_css_light)
      ~solar:(fun _ -> Data.solar_css ^ extra_css_dark)
      ~greyscale:(fun _ -> Data.greyscale_css ^ extra_css_light)

  let cmdliner_term () =
    let open Cmdliner in
    let names =
      [ ("lumen", `Lumen)
      ; ("cerulean", `Cerulean)
      ; ("greyscale", `Greyscale)
      ; ("solarized", `Solar) ] in
    let enums =
      names
      @ [ ("default", `Cerulean)
        ; ("dark", `Solar)
        ; ("random", List.random_element_exn names |> snd) ] in
    Arg.(
      value
        (opt (enum enums) `Cerulean
           (info ["theme"] ~doc:"Theme name of “random”") ))
end

module State = struct
  module Configuration = struct
    type t =
      { port: int
      ; max_connections: int
      ; nodes: Octez_node.set
      ; octez_client_executable: string
      ; fake_tezos_client: bool
      ; exchange_rates_allowed_age: float
      ; exchange_rates_uri: string
      ; theme: Theme.t }
    [@@deriving sexp]

    let make ?(nodes = []) ?(octez_client_executable = "tezos-client")
        ?(exchange_rates_uri = "https://api.tzkt.io/v1/quotes/last")
        ?(theme = `Cerulean) ?(exchange_rates_allowed_age = 500.)
        ?(fake_tezos_client = false) ~port ~max_connections () =
      { port
      ; max_connections
      ; nodes
      ; octez_client_executable
      ; fake_tezos_client
      ; exchange_rates_allowed_age
      ; exchange_rates_uri
      ; theme }
  end

  type accounts = Account.t list [@@deriving sexp]
  type operations = Operation.t list [@@deriving sexp]

  type t =
    { data_dir: string
    ; configuration: Configuration.t
    ; mutable accounts: accounts
    ; mutable operations: operations
    ; main_client: Octez_client.t option
    ; mutable update_loop_errors: exn list
    ; mutable latest_exchange_rates: Exchange_rates.t option }

  type state = t

  let make ?main_client ?(accounts = []) ?(operations = []) ~configuration
      data_dir =
    { data_dir
    ; configuration
    ; main_client
    ; accounts
    ; operations
    ; update_loop_errors= []
    ; latest_exchange_rates= None }

  let get ctxt : t = ctxt#state
  let configuration ctxt = (get ctxt).configuration

  let exchange_rates_allowed_age ctxt =
    (configuration ctxt).exchange_rates_allowed_age

  let exchange_rates_uri ctxt = (configuration ctxt).exchange_rates_uri
  let theme ctxt = (configuration ctxt).theme

  module Read = struct
    let sexp ctxt data_dir subpath =
      let path = Path.(data_dir // subpath) in
      if Io.File.file_exists ctxt path then
        match Io.File.read_lines ctxt path with
        | more_or_less ->
            Some (Sexplib.Sexp.of_string (String.concat ~sep:"\n" more_or_less))
            (* Failure.raise_textf "State: wrong contents at key %S: %a" subpath
               Fmt.Dump.(list string)
               more_or_less *)
        | exception e ->
            Failure.raise_textf "State: missing contents at key %S: %a" subpath
              Exn.pp e
      else None

    let one_line ctxt data_dir subpath =
      let path = Path.(data_dir // subpath) in
      if Io.File.file_exists ctxt path then
        match Io.File.read_lines ctxt path with
        | [one] -> Some one
        | more_or_less ->
            Failure.raise_textf "State: wrong contents at key %S: %a" subpath
              Fmt.Dump.(list string)
              more_or_less
        | exception e ->
            Failure.raise_textf "State: missing contents at key %S: %a" subpath
              Exn.pp e
      else None

    let int ctxt data_dir subpath default =
      match one_line ctxt data_dir subpath with
      | None -> default
      | Some s -> (
        match Int.of_string s with
        | v -> v
        | exception (Failure.F _ as e) -> raise e
        | exception _other ->
            Failure.raise_textf "State: at key %S expecting an integer, not %S"
              subpath s )

    let with_of_sexp ctxt data_dir subpath of_sexp default =
      try
        match sexp ctxt data_dir subpath with
        | None -> default
        | Some s -> of_sexp s
      with e ->
        Failure.raise_textf "Getting sexp in %s/%s: %a" data_dir subpath Exn.pp
          e
  end

  module Write = struct
    let one_line ctxt data_dir subpath value =
      let path = Path.(data_dir // subpath) in
      Io.File.make_path ctxt (Path.dirname path) ;
      Io.File.write_lines ctxt path [value] ;
      ()

    let sexp ctxt data_dir subpath value to_sexp =
      one_line ctxt data_dir subpath
        (value |> to_sexp |> Sexplib.Sexp.to_string_hum)
  end

  module Directory_key_values (P : sig
    type t [@@deriving sexp]

    val subpath : string
    val id : t -> string
    val all : < state: state ; .. > -> t list
  end) =
  struct
    open P

    let subpath = subpath

    let save_one ctxt data_dir acc =
      Write.sexp ctxt data_dir (Path.concat subpath (id acc)) acc sexp_of_t ;
      ()

    let load_one ctxt data_dir path =
      match Option.map ~f:t_of_sexp (Read.sexp ctxt data_dir path) with
      | Some s -> s
      | None -> Failure.raise_textf "Getting %s/%s: not found" data_dir path
      | exception e ->
          Failure.raise_textf "Getting %s/%s: %a" data_dir path Exn.pp e

    let remove_one ctxt id =
      let state = get ctxt in
      Unix.unlink Path.(state.data_dir // subpath // id)

    let save_all ctxt data_dir all =
      List.iter all ~f:(fun acc -> save_one ctxt data_dir acc) ;
      ()

    let load_all ctxt data_dir =
      let l = ref [] in
      let d = Path.(data_dir // subpath) in
      match Caml.Sys.is_directory d with
      | true ->
          Array.iter (Caml.Sys.readdir d) ~f:(fun sub ->
              let p = Path.(subpath // sub) in
              if Caml.Sys.file_exists Path.(data_dir // p) then
                l := load_one ctxt data_dir p :: !l ) ;
          !l
      | false ->
          Failure.raise_textf
            "Problem: %S should be a directory or not exist at all" d
      | exception _ -> []

    let add ctxt v ~add_local =
      let state = get ctxt in
      match
        List.find (all ctxt) ~f:(fun acc -> String.equal (id acc) (id v))
      with
      | Some acc ->
          Failure.raise_textf "Duplicate %s ID: %a %a" subpath Sexp.pp
            (sexp_of_t acc) Sexp.pp (sexp_of_t v)
      | None ->
          add_local ctxt v ;
          save_one ctxt state.data_dir v ;
          (* save ctxt state ; *)
          (* event_added ctxt v ; *)
          ()

    let remove ctxt id ~remove_local =
      remove_local ctxt id ; remove_one ctxt id ; (* save ctxt state ; *)
                                                  ()
  end

  module Accounts = struct
    let all ctxt =
      let state = get ctxt in
      state.accounts

    include Directory_key_values (struct
      let subpath = "account"

      include Account

      let all = all
    end)

    let get_by_id ctxt aid =
      let state = get ctxt in
      match
        List.find state.accounts ~f:String.(fun acc -> Account.id acc = aid)
      with
      | Some s -> s
      | None -> Fmt.failwith "Account not found %s" aid

    let add ctxt acc =
      add ctxt acc ~add_local:(fun ctxt acc ->
          let state = get ctxt in
          state.accounts <- acc :: state.accounts ;
          Events.account_added ctxt acc ;
          () )

    let remove ctxt id =
      remove ctxt id ~remove_local:(fun ctxt id ->
          let state = get ctxt in
          state.accounts <-
            List.filter state.accounts ~f:(fun acc ->
                String.equal acc.id id |> not ) ;
          (* save ctxt state ; *)
          Events.account_removed ctxt id ;
          () )

    let modify ctxt acc =
      let self = get ctxt in
      let open Account in
      self.accounts <-
        List.map self.accounts ~f:(fun a ->
            if String.equal a.id acc.id then
              if Poly.equal a acc then a
              else (
                save_one ctxt self.data_dir acc ;
                Events.account_changed ctxt acc.id ;
                acc )
            else a ) ;
      ()
  end

  module Operations = struct
    let all ctxt =
      let state = get ctxt in
      state.operations

    include Directory_key_values (struct
      let subpath = "operation"

      include Operation

      let all = all
    end)

    let add ctxt acc =
      add ctxt acc ~add_local:(fun ctxt acc ->
          let state = get ctxt in
          state.operations <- acc :: state.operations ;
          Events.operation_added ctxt acc ;
          () )

    let remove ctxt id =
      remove ctxt id ~remove_local:(fun ctxt id ->
          let state = get ctxt in
          state.operations <-
            List.filter state.operations ~f:(fun acc ->
                String.equal acc.id id |> not ) ;
          (* save ctxt state ; *)
          Events.operation_removed ctxt id ;
          () )

    let modify ctxt acc =
      let self = get ctxt in
      let open Operation in
      self.operations <-
        List.map self.operations ~f:(fun a ->
            if String.equal a.id acc.id then (
              if Poly.equal a acc then a
              else
                let updated = {acc with last_update= Unix.gettimeofday ()} in
                save_one ctxt self.data_dir updated ;
                Events.operation_changed ctxt updated.id ;
                updated )
            else a ) ;
      ()
  end

  module Michelson_code = struct
    let as_file ctxt ~code =
      let state = get ctxt in
      let dir = Path.(state.data_dir // "michelson-contracts") in
      let path =
        Path.(dir // Fmt.str "%s.tz" Caml.Digest.(string code |> to_hex)) in
      Io.File.make_path ctxt dir ;
      Io.File.write_lines ctxt path [code] ;
      path
  end

  let load ctxt data_dir =
    let open Read in
    let configuration =
      let default = Configuration.make ~port:8480 ~max_connections:32 () in
      with_of_sexp ctxt data_dir "configuration" Configuration.t_of_sexp default
    in
    let accounts = Accounts.load_all ctxt data_dir in
    let operations = Operations.load_all ctxt data_dir in
    (* with_of_sexp ctxt data_dir "accounts" accounts_of_sexp [] in *)
    let state = make data_dir ~configuration ~accounts ~operations in
    let shell = Io.Shell.default () in
    let file_io = Io.File.default () in
    let clock = Io.Clock.default () in
    let events = Events.create () in
    let ctxt =
      object
        method state = state
        method file_io = file_io
        method shell = shell
        method clock = clock
        method events = events

        method octez_client =
          match configuration.nodes with
          | [] ->
              Failure.raise_textf
                "Cannot make a tezos-client without nodes configured"
          | one :: _ ->
              Octez_client.
                { executable= configuration.Configuration.octez_client_executable
                ; node= one
                ; data_dir= Path.(data_dir // "main-octez-client-data")
                ; faking_it= configuration.Configuration.fake_tezos_client }
      end in
    Octez_client.ensure_initialized ctxt ;
    ctxt

  let save_configuration ctxt data_dir configuration =
    Write.sexp ctxt data_dir "configuration" configuration
      Configuration.sexp_of_t ;
    ()

  let _save ctxt (* unused any more *)
      { data_dir
      ; configuration
      ; main_client= _
      ; accounts
      ; operations
      ; update_loop_errors= _
      ; latest_exchange_rates= _ } =
    Io.File.make_path ctxt data_dir ;
    save_configuration ctxt data_dir configuration ;
    List.iter accounts ~f:(fun acc ->
        Write.sexp ctxt data_dir
          (Fmt.str "account/%s" acc.id)
          acc Account.sexp_of_t ) ;
    List.iter operations ~f:(fun op ->
        Write.sexp ctxt data_dir
          (Fmt.str "operation/%s" op.id)
          op Operation.sexp_of_t ) ;
    (* Option.iter main_client ~f:(fun client ->
        Write.sexp ctxt data_dir "main-client" client Octez_client.sexp_of_t ) ; *)
    ()

  module Exchange_rates = struct
    let set ctxt rates =
      Events.new_exchange_rates ctxt rates ;
      (get ctxt).latest_exchange_rates <- Some rates ;
      ()

    let get_latest ctxt = (get ctxt).latest_exchange_rates

    let timestamp ctxt =
      match (get ctxt).latest_exchange_rates with
      | Some s -> s.Exchange_rates.timestamp
      | None -> Float.min_value
  end

  module RPC = struct
    let a_node ctxt =
      let state = get ctxt in
      List.random_element_exn state.configuration.nodes

    let getf ctxt path = Fmt.kstr (Octez_node.rpc (a_node ctxt)) path

    let postf ctxt ~body path =
      Fmt.kstr (Octez_node.rpc ~meth:(`Post body) (a_node ctxt)) path

    let get_balance ctxt ~address =
      getf ctxt "/chains/main/blocks/head/context/contracts/%s/balance" address
      |> Json.Q.string |> Z.of_string

    let get_manager_key ctxt ~address =
      match
        getf ctxt "/chains/main/blocks/head/context/contracts/%s/manager_key"
          address
      with
      | `Null -> None
      | `String pk -> Some pk
      | other -> Fmt.failwith "get_manager_key: Wrong JSON: %a" Json.pp other

    let storage ctxt ~address =
      let body = Json.C.("unparsing_mode" --> string "Optimized_legacy") in
      postf ctxt ~body
        "/chains/main/blocks/head/context/contracts/%s/storage/normalized"
        address
      |> Tezai_michelson.Untyped.of_json
  end

  let record_loop_error ctxt e =
    let state = get ctxt in
    Fmt.epr "@[<2>LOOP ERROR:@ %a@]%!" Exn.pp e ;
    state.update_loop_errors <- e :: state.update_loop_errors

  let data_dir_cmdliner_term () =
    let open Cmdliner in
    let open Term in
    Arg.(
      let env = env_info ~doc:"State location path." "BABUSO_ROOT" in
      value
        (opt string
           Path.(Caml.Sys.getenv "HOME" // ".babuso")
           (info ["state-path"] ~env ~doc:"Set the State location.") ))

  let configuration_cmdliner_term () =
    let open Cmdliner in
    let open Term in
    const
      (fun
        data_dir
        port
        theme
        max_connections
        octez_client_executable
        nodes
        fake_tezos_client
      ->
        let nodes =
          let open Octez_node in
          List.map nodes ~f:(fun base_url -> {base_url}) in
        let configuration =
          Configuration.make ~port ~max_connections ~octez_client_executable
            ~theme ~fake_tezos_client ~nodes () in
        (data_dir, configuration) )
    $ data_dir_cmdliner_term ()
    $ Arg.(value (opt int 8480 (info ["port"; "P"] ~doc:"Port to listen on.")))
    $ Theme.cmdliner_term ()
    $ Arg.(
        value
          (opt int 32
             (info ["max-connections"] ~doc:"Maximum number of connections.") ))
    $ Arg.(
        value
          (opt string "tezos-client"
             (info
                ["octez-client-executable"]
                ~doc:"Path to tezos-client binary." ) ))
    $ Arg.(
        value
          (opt_all string []
             (info ["octez-node"] ~doc:"Add an Octez node base-URL.") ))
    $ Arg.(
        value
          (flag
             (info ["fake-octez-client"]
                ~doc:"Try to mockup tezos-client responses." ) ))

  let loading_cmdliner_term () =
    let open Cmdliner in
    let open Term in
    const (fun data_dir ->
        let ctxt =
          object
            method file_io = Io.File.default ()
          end in
        load ctxt data_dir )
    $ data_dir_cmdliner_term ()
end

module Httpd = struct
  include Tiny_httpd

  let respond_error ?(code = 404) ?req pp =
    let body =
      Fmt.str "%a" Pp.to_fmt
        Docpp.(
          vbox
            ( box
                ( match code with
                | 404 -> textf "Hello 404 my old friend"
                | other -> textf "Error %d" other )
            +++ box pp
            ++ Option.value_map req ~default:nop ~f:(fun req ->
                   cut
                   ++ box ~indent:2 (text "Request:" +++ of_fmt Request.pp req) )
            )) in
    dbgf "Http error:\n%s" body ;
    Response.make_string
      ~headers:[("Content-Type", "text/plain")]
      (Error (404, body))

  let respond_404 ?req pp = respond_error ~code:404 ?req pp

  let handle_exceptions ?req f =
    try f () with
    | Failure.F {doc; code} ->
        let code = match code with 404 -> 404 | _ -> 500 in
        respond_error ~code doc ?req
    | e ->
        respond_error ~code:500
          Docpp.(
            text "There was an error:"
            +++
            match e with
            | Failure s -> verbatim s
            | other -> Fmt.kstr verbatim "%a" Exn.pp other)

  let header_argument query =
    match List.Assoc.find query "header" ~equal:String.equal with
    | Some "none" -> `None
    | None -> `Default
    | Some other ->
        Failure.raise Docpp.(textf "Wrong 'header' argument: %S" other)
end

module Contract_call = struct
  let parameter parameters s =
    List.Assoc.find parameters s ~equal:String.equal
    |> function
    | Some s -> Variable.Value.to_michelson s
    | None -> Fmt.failwith "Missing parameter: %S" s

  let variable variables s =
    Variable.(Record.get variables s |> Value.to_michelson)

  module Generic_multisig = struct
    let update_keys_payload _ ~parameters =
      Fmt.str "(Right (Pair %s %s))"
        (parameter parameters "threshold")
        (parameter parameters "keys")

    let lambda_payload _ ~parameters =
      Fmt.str "(Left %s)" (parameter parameters "lambda")

    let thing_to_pack_and_sign ctxt ~parameters ~variables ~address payload =
      ignore parameters ;
      let chain_id =
        State.RPC.getf ctxt "/chains/main/chain_id" |> Json.Q.string in
      Fmt.str "(Pair (Pair %S %S) (Pair %s %s))" chain_id address
        (variable variables "counter")
        payload

    let arg ~payload ~parameters ~variables =
      let signature_opts = parameter parameters "signatures" in
      Fmt.str "(Pair (Pair %s %s) %s )"
        (variable variables "counter")
        payload signature_opts
  end
end

module Indexer = struct
  let balance ctxt ~address =
    let balance = State.RPC.get_balance ctxt ~address in
    balance

  let manager_key ctxt ~address = State.RPC.get_manager_key ctxt ~address

  let generic_multisig_counter_threshold_keys ctxt ~address =
    let storage = State.RPC.storage ctxt ~address in
    dbgf "Storage: %a" Tezai_michelson.Untyped.pp storage ;
    let open Tezai_michelson.Untyped.M in
    match storage with
    | Prim
        ( _
        , "Pair"
        , [Int (_, c); Prim (_, "Pair", [Int (_, thr); Seq (_, keys)], _)]
        , _ ) ->
        ( c
        , thr
        , List.map keys ~f:(function
            | Bytes (_, s) when Char.(Bytes.get s 0 = '\x00') ->
                Bytes.to_string s
                |> String.chop_prefix_exn ~prefix:"\x00"
                |> Tezai_base58_digest.Identifier.Ed25519.Public_key.encode
            | Bytes (_, _) ->
                Fmt.failwith "Only Ed25519 for now (contract: %s)" address
            | String (_, s) -> s
            | _ -> assert false ) )
    | Prim _ | Int (_, _) | String (_, _) | Bytes (_, _) | Seq (_, _) ->
        assert false

  let generic_multisig ctxt ~address =
    let counter, threshold, keys =
      generic_multisig_counter_threshold_keys ctxt ~address in
    let specification = Smart_contract.generic_multisig in
    Originated_contract.make address ~specification
      ~variables:
        Variable.(
          Record.comb
            [ ("counter", Value.nat counter)
            ; ("threshold", Value.nat threshold)
            ; ("keys", Value.key_list keys) ])

  let any_kt1 _ctxt ~address =
    Originated_contract.make address ~variables:Variable.Record.empty
      ~specification:
        (Smart_contract.make [] ~fully_recognized:false ~can_receive_funds:false)

  let identify_address ctxt ~address =
    match String.prefix address 3 with
    | "KT1" -> (
        let attempts = [generic_multisig; any_kt1] in
        match
          List.find_map attempts ~f:(fun attempt ->
              match attempt ctxt ~address with
              | backend -> Some backend
              | exception e ->
                  dbgf "Attempt Failed: %a" Exn.pp e ;
                  None )
        with
        | Some s -> Account.Status.originated_contract s
        | None -> Failure.raise_textf "Cannot identify address: %S" address )
    | "tz1" | "tz2" | "tz3" ->
        Account.Status.friend (Friend.make ~public_key_hash:address ())
    | _ -> Failure.raise_textf "Cannot identify address: %S" address
end

module Service = struct
  let index _ =
    let mime_type = ("Content-Type", "text/html") in
    Httpd.Response.make_string ~headers:[mime_type] (Ok Data.index_html)

  let main_js _ =
    let mime_type = ("Content-Type", "text/javascript") in
    Httpd.Response.make_string ~headers:[mime_type] (Ok Data.main_js)

  let bootstrap_css ctxt =
    let mime_type = ("Content-Type", "text/css") in
    let css = State.theme ctxt |> Theme.css in
    Httpd.Response.make_string ~headers:[mime_type] (Ok css)

  let ui_protocol ctxt ~body =
    let open Protocol in
    let mime_type = ("Content-Type", "text/plain") in
    let headers =
      [ mime_type
      ; (* https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control *)
        ("Cache-Control", "no-store") ] in
    let respond_down_message m =
      let body = Message.Down.(m |> sexp_of_t) |> Sexp.to_string_mach in
      Httpd.Response.make_string ~headers (Ok body) in
    match Sexplib.Sexp.of_string body |> Message.Up.t_of_sexp with
    | Get_configuration ->
        respond_down_message
          (Message.Down.Blob
             ( State.configuration ctxt |> State.Configuration.sexp_of_t
             |> Sexp.to_string_hum ) )
    | List_connected_ledgers ->
        let lines = Octez_client.command ctxt ["list"; "connected"; "ledgers"] in
        let parsed =
          List.fold lines ~init:([], []) ~f:(fun (cur, all) line ->
              let words = String.split_on_chars line ~on:[' '; '`'] in
              match words with
              | "##" :: "Ledger" :: "" :: new_one :: _ ->
                  (* match String.chop_prefix line ~prefix:"## Ledger `" with
                     | Some new_one -> *)
                  ( [String.chop_suffix_if_exists new_one ~suffix:"`"]
                  , cur :: all )
              | "Found" :: "a" :: "Tezos" :: "Wallet" :: version :: _ ->
                  (version :: cur, all)
              | _ -> (cur, all) )
          |> fun (last, all) ->
          List.filter_map (last :: all) ~f:(function
            | [] -> None
            | [animals] ->
                Some (Message.Down.ledger ~animals ~wallet_app_version:None)
            | [version; animals] ->
                Some
                  (Message.Down.ledger ~animals
                     ~wallet_app_version:(Some version) )
            | _ ->
                Failure.raise_textf "Bug parsing ledgers: %a"
                  Fmt.Dump.(list string)
                  lines ) in
        respond_down_message Message.Down.(Ledgers parsed)
        (* Failure.raise ~code:500 Docpp.(textf "NOT IMPLEMENTED") *)
    | All_accounts ->
        respond_down_message
          Message.Down.(Account_list (State.Accounts.all ctxt))
    | All_operations ->
        respond_down_message
          (Message.Down.operation_list (State.Operations.all ctxt))
    | Get_events since ->
        dbgf "Get_events %f called!" since ;
        let waited_on_events = Events.get ctxt ~since in
        let events, ts =
          let ts = ref Float.min_value in
          let evs =
            List.map waited_on_events ~f:(fun {timestamp; content} ->
                ts := Float.max timestamp !ts ;
                content ) in
          (List.dedup_and_sort ~compare:Poly.compare evs, !ts) in
        let msg = Message.Down.events events ts in
        dbgf "Get_events %f woken up with %a!" since Sexp.pp
          (Message.Down.sexp_of_t msg) ;
        respond_down_message msg
    | Get_exchange_rates ->
        let er = State.Exchange_rates.get_latest ctxt in
        respond_down_message (Message.Down.exchange_rates er)
    | Delete_account id ->
        State.Accounts.remove ctxt id ;
        respond_down_message Message.Down.Done
    | Create_generic_multisig_account {name; keys; threshold; gas_wallet} ->
        let account_id = Fresh_id.make () in
        let operation =
          let id = Fresh_id.make () in
          let specification = Smart_contract.generic_multisig in
          let initialization =
            Variable.(
              Map.of_list
                [ ("counter", Value.nat Z.zero)
                ; ("threshold", Value.nat (Z.of_int threshold))
                ; ("keys", Value.key_list keys) ]) in
          let order =
            Operation.Specification.origination ~specification ~initialization
              ~account:account_id ~gas_wallet in
          let last_update = Unix.gettimeofday () in
          Operation.make ~id ~order ~last_update
            ~status:Operation.Status.ordered in
        State.Operations.add ctxt operation ;
        let kind =
          Account.Status.contract_to_originate
            ~operation:(Operation.id operation) in
        State.Accounts.add ctxt
          (Account.make ~id:account_id ~display_name:name ~balance:Z.zero kind) ;
        respond_down_message Message.Down.Done
    | Create_smart_contract {name; gas_wallet; entrypoints; initialization} ->
        let account_id = Fresh_id.make () in
        let operation =
          let id = Fresh_id.make () in
          let specification =
            let can_receive_funds =
              List.exists entrypoints ~f:(function
                | {Entrypoint.control= Anyone; ability= Do_nothing; _} -> true
                | _ -> false ) in
            Smart_contract.make ~fully_recognized:true ~can_receive_funds
              entrypoints in
          let order =
            Operation.Specification.origination ~specification ~initialization
              ~account:account_id ~gas_wallet in
          let last_update = Unix.gettimeofday () in
          Operation.make ~id ~order ~last_update
            ~status:Operation.Status.ordered in
        State.Operations.add ctxt operation ;
        let kind =
          Account.Status.contract_to_originate
            ~operation:(Operation.id operation) in
        State.Accounts.add ctxt
          (Account.make ~id:account_id ~display_name:name ~balance:Z.zero kind) ;
        respond_down_message Message.Down.Done
    | Order_operation {specification} ->
        let operation =
          let id = Fresh_id.make () in
          let last_update = Unix.gettimeofday () in
          Operation.make ~id ~last_update ~order:specification
            ~status:Operation.Status.ordered in
        State.Operations.add ctxt operation ;
        respond_down_message Message.Down.Done
    | Make_blob_for_signature {target_contract_id; entrypoint; parameters} -> (
        let account = State.Accounts.get_by_id ctxt target_contract_id in
        match account.state with
        | Account.Status.Key_pair _ | Account.Status.Friend _
         |Account.Status.Contract_to_originate _ ->
            Fmt.failwith "%S is not a contract" target_contract_id
        | Account.Status.Originated_contract
            {specification= Generic_multisig; address; variables} ->
            let full_thing_to_pack =
              let payload =
                match entrypoint with
                | "main/lambda" ->
                    Contract_call.Generic_multisig.lambda_payload ctxt
                      ~parameters
                | "main/update_keys" ->
                    Contract_call.Generic_multisig.update_keys_payload ctxt
                      ~parameters
                | other ->
                    Fmt.failwith "Entrypoint %S not found for %s" other
                      target_contract_id in
              Contract_call.Generic_multisig.thing_to_pack_and_sign ctxt
                ~parameters ~variables ~address payload
              (* let chain_id =
                   State.RPC.getf ctxt "/chains/main/chain_id" |> Json.Q.string
                 in
                 Fmt.str "(Pair (Pair %S %S) (Pair %s %s))" chain_id address
                   Variable.(
                     Record.get variables "counter" |> Value.to_michelson)
                   payload *) in
            let blob =
              let lines =
                Octez_client.command ctxt
                  [ "hash"
                  ; "data"
                  ; full_thing_to_pack
                  ; "of"
                  ; "type"
                  ; "(pair (pair chain_id address) (pair nat (or (lambda unit \
                     (list operation))(pair nat (list key)) )))" ] in
              match
                List.find_map lines ~f:(fun l ->
                    String.chop_prefix l ~prefix:"Raw packed data: 0x" )
              with
              | None ->
                  Fmt.failwith "Packing failed %a" Fmt.Dump.(list string) lines
              | Some s -> Hex.to_string (`Hex s) in
            respond_down_message (Message.Down.blob blob)
        | Account.Status.Originated_contract _ ->
            Fmt.failwith "%S is not a supported contract yet" target_contract_id
        )
    | Sign_blob {account; blob} ->
        let bytes_hex =
          let (`Hex hx) = Hex.of_string blob in
          "0x" ^ hx in
        let signature =
          let lines =
            Octez_client.command ctxt
              ["sign"; "bytes"; bytes_hex; "for"; account] in
          match lines with
          | [one] -> begin
            match String.chop_prefix one ~prefix:"Signature: " with
            | Some s -> s
            | None ->
                Fmt.failwith "Could not parse sign output: %a"
                  Fmt.Dump.(string)
                  one
          end
          | _ ->
              Fmt.failwith "Could not parse sign output: %a"
                Fmt.Dump.(list string)
                lines in
        respond_down_message (Message.Down.signature signature)
    | Import_account {name; kind} -> (
      match kind with
      | `Address address ->
          let balance = Indexer.balance ctxt ~address in
          let kind = Indexer.identify_address ctxt ~address in
          let id = Fresh_id.make () in
          State.Accounts.add ctxt
            (Account.make ~id ~display_name:name ~balance kind) ;
          respond_down_message Message.Down.Done
      | (`Ledger_uri _ | `Unencrypted_uri _) as kind ->
          let id = Fresh_id.make () in
          let uri =
            match kind with `Ledger_uri uri | `Unencrypted_uri uri -> uri in
          let lines =
            Octez_client.command ctxt
              ["import"; "secret"; "key"; id; uri; "--force"] in
          dbgf "Import result: %a" Fmt.Dump.(list string) lines ;
          let lines = Octez_client.command ctxt ["show"; "address"; id] in
          dbgf "Show result: %a" Fmt.Dump.(list string) lines ;
          let public_key_hash, public_key =
            match List.map lines ~f:(fun l -> String.split ~on:' ' l) with
            | [["Hash:"; h]; ["Public"; "Key:"; p]] -> (h, p)
            | _ ->
                Failure.raise_textf "Failed to parse public-key(-hash): %a"
                  Fmt.Dump.(list string)
                  lines in
          let backend =
            Key_pair.make ~public_key ~public_key_hash
              ( match kind with
              | `Ledger_uri uri -> Ledger {uri}
              | `Unencrypted_uri uri -> Unencrypted {private_uri= uri} ) in
          State.Accounts.add ctxt
            (Account.make ~id ~display_name:name
               (Account.Status.key_pair backend) ) ;
          respond_down_message Message.Down.Done )
    | exception e ->
        Failure.raise ~code:500
          Docpp.(Fmt.kstr text "Error parsing message: %a" Exn.pp e)
end

module Update_loop = struct
  let protect ctxt name f =
    dbgf "Update_loop/%s starts" name ;
    match f ctxt with
    | () -> ()
    | exception e ->
        dbgf "@[<4>Update_loop/%s:@ %a@]" name Exn.pp e ;
        State.record_loop_error ctxt e ;
        ()

  let exchange_rates ctxt =
    let latest_quotes = State.Exchange_rates.timestamp ctxt in
    let allowed_age = State.exchange_rates_allowed_age ctxt in
    if Float.(Unix.gettimeofday () > latest_quotes + allowed_age) then (
      let tzkt_quotes = State.exchange_rates_uri ctxt in
      let quotes =
        let j = Json_web_api.call tzkt_quotes in
        dbgf "quotes: %a" Json.pp j ;
        let float k = Json.Q.float_field j ~k in
        Exchange_rates.
          { timestamp= Unix.gettimeofday ()
          ; btc= float "btc"
          ; usd= float "usd"
          ; eth= float "eth"
          ; eur= float "eur" } in
      State.Exchange_rates.set ctxt quotes ;
      () ) ;
    ()

  let with_alerting ctxt ~reason gas_account f =
    match gas_account with
    | {Account.state= Key_pair {backend= Ledger _; _}; display_name; _} -> begin
        let alert_id =
          Events.add_alert ctxt
            (Events.Ev.Alert.ledger_wants_human_intervention
               ~ledger_id:(Account.id gas_account) ~ledger_name:display_name
               ~reason ) in
        match f () with
        | v ->
            Events.remove_alert ctxt alert_id ;
            v
        | exception e ->
            Events.remove_alert ctxt alert_id ;
            raise e
      end
    | _ -> f ()

  let ensure_key_pair ctxt acc =
    match acc with
    | {Account.state= Key_pair {backend; _}; _} -> begin
      try
        Octez_client.command ctxt ["show"; "address"; Account.id acc] |> ignore
      with _ ->
        let uri =
          match backend with
          | Unencrypted {private_uri} -> private_uri
          | Ledger {uri} -> uri in
        with_alerting ctxt
          ~reason:(Fmt.str "Importing secret key %S into tezos-client." uri) acc
          (fun () ->
            Octez_client.command ctxt
              ["import"; "secret"; "key"; acc.id; uri; "--force"]
            |> ignore )
    end
    | _ ->
        Failure.raise
          Docpp.(textf "Account %S is not a key-pair" acc.display_name)

  let do_origination ctxt ~specification ~initialization ~gas_wallet ~account_id
      =
    let code, init_record =
      match specification with
      | Smart_contract.Generic_multisig ->
          ( Data.generic_multisig_tz
          , Variable.Record.of_map
              initialization (* Variable.Map.to_michelson initialization *)
              ~structure:["counter"; "threshold"; "keys"] )
      | Smart_contract.Custom {entrypoints; _} ->
          let `Code mich, `Init_example _, `Storage_fields_order structure =
            Law_office.make_contract entrypoints in
          (mich, Variable.Record.of_map initialization ~structure) in
    let gas_account = State.Accounts.get_by_id ctxt gas_wallet in
    let () = ensure_key_pair ctxt gas_account in
    let code_to_run = State.Michelson_code.as_file ctxt ~code in
    let _lines =
      with_alerting ctxt ~reason:(Fmt.str "Origination of  %s" account_id)
        gas_account (fun () ->
          Octez_client.command ctxt
            [ "originate"
            ; "contract"
            ; account_id
            ; "transferring"
            ; "0"
            ; "from"
            ; gas_wallet
            ; "running"
            ; code_to_run
            ; "--init"
            ; Variable.Record.to_michelson init_record
            ; "--burn-cap"
            ; "10" ] ) in
    let address =
      Octez_client.command ctxt ["show"; "known"; "contract"; account_id]
      |> String.concat ~sep:"" in
    (address, init_record)

  let accounts ctxt =
    let all_accounts = State.Accounts.all ctxt in
    List.iter all_accounts ~f:(fun acc ->
        (* let open Account in *)
        protect ctxt (Fmt.str "account/%s(%s)" acc.id acc.display_name)
          (fun ctxt ->
            match acc.state with
            | Friend fr ->
                let balance =
                  Some (Indexer.balance ctxt ~address:fr.public_key_hash) in
                let pk =
                  match fr.public_key with
                  | Some s -> Some s
                  | None -> Indexer.manager_key ctxt ~address:fr.public_key_hash
                in
                let acc =
                  {acc with balance; state= Friend {fr with public_key= pk}}
                in
                State.Accounts.modify ctxt acc ;
                ()
            | Key_pair kp ->
                let balance =
                  Some (Indexer.balance ctxt ~address:kp.public_key_hash) in
                let acc = {acc with balance} in
                State.Accounts.modify ctxt acc ;
                ()
            | Originated_contract {address; specification; variables= _} ->
                let balance = Some (Indexer.balance ctxt ~address) in
                let state =
                  match specification with
                  | Smart_contract.Custom _ -> acc.state
                  | Smart_contract.Generic_multisig ->
                      Originated_contract
                        (Indexer.generic_multisig ctxt ~address) in
                let acc = {acc with balance; state} in
                State.Accounts.modify ctxt acc ;
                ()
            | Contract_to_originate {operation= _} -> () ) ) ;
    ()

  let do_transfer ctxt ~source ~amount ~destination ~entrypoint ~parameters =
    let source_account = State.Accounts.get_by_id ctxt source in
    dbgf "Not doing transfer: %a" Z.pp_print amount ;
    let () = ensure_key_pair ctxt source_account in
    let entrypoint, arg =
      match entrypoint with
      | "main/update_keys" ->
          let counter, _, _ =
            Indexer.generic_multisig_counter_threshold_keys ctxt
              ~address:destination in
          let variables =
            Variable.Record.comb [("counter", Variable.Value.nat counter)] in
          ( "main"
          , Contract_call.Generic_multisig.(
              arg
                ~payload:(update_keys_payload ctxt ~parameters)
                ~variables ~parameters) )
      | "main/lambda" ->
          let counter, _, _ =
            Indexer.generic_multisig_counter_threshold_keys ctxt
              ~address:destination in
          let variables =
            Variable.Record.comb [("counter", Variable.Value.nat counter)] in
          ( "main"
          , Contract_call.Generic_multisig.(
              arg
                ~payload:(lambda_payload ctxt ~parameters)
                ~variables ~parameters) )
      | other -> (other, Variable.Record.(comb parameters |> to_michelson))
    in
    let _lines =
      with_alerting ctxt source_account
        ~reason:
          (Fmt.str "Transfer %s → %s" source_account.display_name destination)
        (fun () ->
          Octez_client.command ctxt
            [ "transfer"
            ; Mutez.to_tez_string amount
            ; "from"
            ; Account.id source_account
            ; "to"
            ; destination
            ; "--burn-cap"
            ; "1"
            ; "--entrypoint"
            ; entrypoint
            ; "--arg"
            ; arg ] ) in
    ()

  let operations ctxt =
    let all_operations = State.Operations.all ctxt in
    List.iter all_operations ~f:(fun op ->
        (* let open Account in *)
        protect ctxt (Fmt.str "operation/%s" op.id) (fun _ctxt ->
            match op.status with
            | Ordered -> begin
              match Operation.order op with
              | Operation.Specification.Origination
                  {account; specification; initialization; gas_wallet} -> begin
                  let tzop =
                    Operation.Status.make_tezos_operation
                      ~operation_hash:"ooFakeFakeFake"
                      ~base_block_hash:"BLockFakeFakeFake" () in
                  State.Operations.modify ctxt
                    {op with status= Operation.Status.work_in_progress ~op:None} ;
                  match
                    do_origination ctxt ~specification ~initialization
                      ~gas_wallet ~account_id:account
                  with
                  | exception e ->
                      State.Operations.modify ctxt
                        { op with
                          status=
                            Fmt.kstr Operation.Status.failed "meh: %a" Exn.pp e
                        }
                  | address, init_record ->
                      let acc = State.Accounts.get_by_id ctxt account in
                      let state =
                        Account.Status.originated_contract
                          (Originated_contract.make ~variables:init_record
                             ~specification address ) in
                      State.Accounts.modify ctxt {acc with state} ;
                      State.Operations.modify ctxt
                        {op with status= Operation.Status.success ~op:tzop} ;
                      ()
                end
              | Operation.Specification.Transfer
                  {source; amount; destination; entrypoint; parameters} -> begin
                  let tzop =
                    Operation.Status.make_tezos_operation
                      ~operation_hash:"ooFakeFakeFake"
                      ~base_block_hash:"BLockFakeFakeFake" () in
                  State.Operations.modify ctxt
                    {op with status= Operation.Status.work_in_progress ~op:None} ;
                  match
                    do_transfer ctxt ~source ~amount ~destination ~parameters
                      ~entrypoint
                  with
                  | exception e ->
                      State.Operations.modify ctxt
                        { op with
                          status=
                            Fmt.kstr Operation.Status.failed "meh: %a" Exn.pp e
                        }
                  | _ ->
                      State.Operations.modify ctxt
                        {op with status= Operation.Status.success ~op:tzop} ;
                      ()
                end
            end
            | Work_in_progress _ -> Fmt.failwith "wip operation"
            | Success _ | Failed _ -> () ) ) ;
    ()

  let start ctxt =
    let rec loop nth =
      let run () =
        dbgf "update_loop[%d]: starting" nth ;
        if nth % 10 = 0 then protect ctxt "exchange_rates" exchange_rates ;
        operations ctxt ;
        if nth % 3 = 0 then accounts ctxt ;
        () in
      let continue () =
        (* List.map ["usd"; "eur"; "btc"] ~f:(fun c ->
              let x = Get.float_field c quotes in
           ) ] ) *)
        dbgf "update_loop[%d]: sleeping now" nth ;
        Unix.sleepf 2. ;
        loop (nth + 1) in
      match run () with
      | () -> continue ()
      | exception e ->
          State.record_loop_error ctxt e ;
          continue () in
    let (_ : Thread.t) = Thread.create loop 0 in
    ()
end

let start_server ?(debug = false) ctxt () =
  let State.Configuration.{port; max_connections; _} =
    State.configuration ctxt in
  Update_loop.start ctxt ;
  dbgf "Starting server :%d (max: %d)" port max_connections ;
  Httpd._enable_debug debug ;
  let server = Httpd.create ~port ~max_connections () in
  Tiny_httpd_camlzip.setup ~compress_above:1024 ~buf_size:(1024 * 1024) server ;
  Httpd.set_top_handler server (fun req ->
      Httpd.respond_404 ~req Docpp.(text "Unkonwn request path.") ) ;
  Httpd.add_route_handler ~meth:`GET server
    Httpd.Route.(rest_of_path)
    (fun path req ->
      Httpd.handle_exceptions ~req (fun () ->
          (* let header = Httpd.header_argument query in *)
          match Uri.pct_decode path with
          | "" | "index" | "index.html" -> Service.index ctxt
          | "main-client.js" -> Service.main_js ctxt
          | "bootstrap.css" -> Service.bootstrap_css ctxt
          | "please/die" -> Caml.exit 0
          | path -> Failure.raise ~code:500 Docpp.(textf "Wrong path: %S" path) )
      ) ;
  Httpd.add_route_handler ~meth:`POST server
    Httpd.Route.(exact "ui-protocol" @/ return)
    (fun req ->
      Httpd.handle_exceptions ~req (fun () ->
          Service.ui_protocol ctxt ~body:(Httpd.Request.body req) ) ) ;
  Fmt.pr "listening on http://%s:%d\n%!" (Httpd.addr server) (Httpd.port server) ;
  match Httpd.run server with Ok () -> () | Error e -> raise e

let start_server_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun ctxt () -> start_server ctxt ())
    $ State.loading_cmdliner_term ()
    $ const () in
  (term, info "start-server" ~doc:"Start the webserver.")

let octez_client_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun ctxt args ->
        let res = Octez_client.command ctxt args in
        List.iter ~f:(Fmt.pr "%s\n%!") res )
    $ State.loading_cmdliner_term ()
    $ Arg.(value (pos_all string [] (info [] ~doc:"ARGUMENTS"))) in
  (term, info "octez-client" ~doc:"Call commands with the default client.")

let configure_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun (data_dir, configuration) () ->
        let ctxt =
          object
            method file_io = Io.File.default ()
          end in
        State.save_configuration ctxt data_dir configuration ;
        () )
    $ State.configuration_cmdliner_term ()
    $ const () in
  (term, info "configure" ~doc:"Write the configuration.")

let self_test_command () =
  let open Cmdliner in
  let open Term in
  (* let resultf fmt = Fmt.kstr (Fmt.epr "SELF-TEST: %s\n%!") fmt in *)
  let term =
    const (fun ctxt () ->
        Fmt.epr "Smartml config: %a\n%!" SmartML.Config.pp
          Tezai_smartml_generation.Compile.default_type_checking_config ;
        Fmt.epr "Contracts:\n%!" ;
        let tmp = "_build/generated-contracts/" in
        Io.File.make_path ctxt tmp ;
        let base = "_build/mockup-client/" in
        Io.Shell.exec_to_string_list ctxt ["rm"; "-fr"; base] |> ignore ;
        (* Io.File.make_path ctxt base ; *)
        let mockup args =
          let exec = (Octez_client.get ctxt).executable in
          Io.Shell.exec_to_string_list ctxt
            ([exec; "--base-dir"; base; "--mode"; "mockup"] @ args) in
        let mockup_unit args =
          let (_ : string list) = mockup args in
          () in
        mockup_unit
          [ "--protocol"
          ; "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"
          ; "create"
          ; "mockup" ] ;
        List.iter (Law_office.valid_examples ()) ~f:(fun (n, eps) ->
            let `Code mich, `Init_example init, `Storage_fields_order _ =
              Law_office.make_contract eps in
            let path = State.Michelson_code.as_file ctxt ~code:mich in
            Fmt.epr "- Created `%s`\n%!" path ;
            Fmt.epr "    - Init: `%s`\n%!" init ;
            let _origination =
              mockup
                [ "originate"
                ; "contract"
                ; n
                ; "transferring"
                ; "10"
                ; "from"
                ; "bootstrap1"
                ; "running"
                ; path
                ; "--init"
                ; init
                ; "--burn-cap"
                ; "10" ] in
            let address =
              mockup ["show"; "known"; "contract"; n] |> String.concat ~sep:""
            in
            Fmt.epr "    - Origination: %s\n" address ;
            () ) ;
        (*
        resultf "get-balance: %a" Z.pp_print
          (State.RPC.get_balance ctxt
             ~address:"KT1WSUAUrqBaDou2DPkysu4nNQUoUyHgtRKz" ) ;
        resultf "storage: %a" Tezai_michelson.Untyped.pp
          (State.RPC.storage ctxt
             ~address:"KT1WSUAUrqBaDou2DPkysu4nNQUoUyHgtRKz" ) ;
 *)
        () )
    $ State.loading_cmdliner_term ()
    $ const () in
  (term, info "self-test" ~doc:"Test itself and its configuration.")

let main () =
  let open Cmdliner in
  let version = "0.0.0" in
  let help =
    Term.
      ( ret (const (`Help (`Auto, None)))
      , info "babuso" ~version ~doc:"Bank, But Self-Operated" ) in
  Term.exit
    (Failure.run_or_die (fun () ->
         Term.eval_choice ~catch:false
           (help : unit Term.t * _)
           [ start_server_command ()
           ; configure_command ()
           ; octez_client_command ()
           ; self_test_command () ] ) )

let () =
  Debug_app.on := true ;
  main ()
