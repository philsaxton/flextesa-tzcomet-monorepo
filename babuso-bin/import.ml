include Babuso_lib.Import

module Path = struct
  include Caml.Filename

  let ( // ) = concat
end

module Date = struct
  open Ptime

  type t = {time: Ptime.t; tz: int}

  let opt_exn msg o = Option.value_exn ~message:(Fmt.str "Date: %s" msg) o

  let pp_debug ppf {time; tz} =
    Fmt.pf ppf "{time: %s; tz: %d}" (Ptime.to_rfc3339 time) tz

  let now ?(tz_s = 0) () =
    let time =
      Unix.gettimeofday () +. Float.of_int tz_s
      |> Ptime.of_float_s |> opt_exn "now failed" in
    {time; tz= tz_s}

  module Local = struct
    let default_tz_s = ref 0

    let () =
      let open Unix in
      let now = gettimeofday () in
      let local = localtime now |> mktime |> fst in
      let gm = gmtime now |> mktime |> fst in
      let tz_seconds = local -. gm in
      default_tz_s := Float.to_int tz_seconds

    let now () = now ~tz_s:!default_tz_s ()
  end

  let to_short_string ?(with_tz = true) {time; tz} =
    let (y, m, d), ((hh, mm, ss), _) = to_date_time time in
    let ms = frac_s time |> Span.to_float_s in
    Fmt.str "%04d%02d%02d-%02d%02d%02d.%03d%s" y m d hh mm ss
      (ms *. 1000. |> Float.to_int)
      ( if with_tz then
        Fmt.str "%s%02d%02d"
          (if tz >= 0 then "+" else "-")
          (abs tz / 60 / 60)
          (abs tz / 60 % 60)
      else "" )
end

module Debug (P : sig
  val name : string
end) =
struct
  let on =
    ref
      ( try String.equal "true" (Fmt.kstr Sys.getenv_exn "%s_debug" P.name)
        with _ -> false )

  let color =
    ref
      ( match Fmt.kstr Sys.getenv_exn "%s_debug_color" P.name with
      | "none" -> None
      | color -> Some color
      | exception _ -> Some "30m" )

  module O = struct
    let dbg (fmt : Caml.Format.formatter -> unit -> unit) =
      let color_start, color_end =
        match !color with
        | None -> ("", "")
        | Some c -> (Fmt.str "\x1b[%s" c, "\x1b[0m") in
      if !on then (
        let b = Buffer.create 512 in
        let formatter = Caml.Format.formatter_of_buffer b in
        Caml.Format.(
          let {max_indent; margin} = pp_get_geometry err_formatter () in
          pp_set_geometry formatter ~max_indent ~margin) ;
        Caml.Format.fprintf formatter
          "@[@<0>%s@[<hov 2>[%s-debug:%s]@ @[%a@]@]@]%s@?\n%!" color_start
          P.name
          Date.(Local.now () |> to_short_string)
          fmt () color_end ;
        let bytes = Buffer.contents_bytes b in
        let (_ : int) =
          (* Unix.write is more atomic. *)
          Unix.write Unix.stderr bytes 0 (Bytes.length bytes) in
        () )

    let dbgp pp = dbg (fun ppf () -> Pp.to_fmt ppf pp)
    let dbgf fmt = Fmt.kstr (fun s -> dbgp (Pp.verbatim s)) fmt
  end

  module Timing = struct
    open O

    let measure name f =
      let start = Unix.gettimeofday () in
      dbgp Docpp.(textf "Start %s %f" name start) ;
      Exn.protect ~f ~finally:(fun () ->
          let ending = Unix.gettimeofday () in
          dbgp Docpp.(textf "End %s %f" name (ending -. start)) )
  end
end
