open! Import
open! Babuso_lib.Wallet_data

let make_contract entrypoints =
  let open Tezai_smartml_generation in
  let open Construct in
  let add_request_checking_type name list_ref ~field f ~typ t v ~t_equal =
    match List.find !list_ref ~f:(fun x -> String.equal (field x) f) with
    | None -> list_ref := v :: !list_ref
    | Some tup when t_equal (typ tup) t -> ()
    | Some _ -> Failure.raise_textf "BUG: %s field type mismatch: %S" name f
  in
  let storage_requests = ref [] in
  let add_storage_request field t v =
    add_request_checking_type "Storage" storage_requests
      ~field:(fun (f, _, _) -> f)
      ~t_equal:T.equal field
      ~typ:(fun (_, t, _) -> t)
      t (field, t, v) in
  let entrypoints =
    List.map entrypoints ~f:(fun ({Entrypoint.name; control; ability} as ep) ->
        let not_implemented fmt =
          Fmt.kstr
            (fun s ->
              Failure.raise
                Docpp.(
                  text "NOT IMPLEMENTED:" +++ text s
                  +++ itemize
                        [ text "Control" +++ sexpable Control.sexp_of_t control
                        ; text "Ability" +++ sexpable Ability.sexp_of_t ability
                        ]) )
            fmt in
        let open Constant_or_variable in
        let open Control in
        let open Ability in
        let parameter_requests = ref [] in
        let add_parameter_request field t =
          add_request_checking_type "Parameter" parameter_requests
            ~t_equal:Variable.Type.equal
            ~field:(fun (f, _) -> f)
            field
            ~typ:(fun (_, t) -> t)
            t (field, t) in
        let constant_or_variable cov ~make_constant ~t ~example =
          match cov with
          | Constant m -> E.constant (make_constant m)
          | Variable varname ->
              add_storage_request varname t (make_constant example) ;
              E.storage ~field:varname () in
        let do_check =
          match control with
          | Anyone -> []
          | Sender_is sender_spec ->
              let compare_to =
                constant_or_variable sender_spec ~make_constant:L.address
                  ~t:T.address ~example:"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
              in
              (* match sender_spec with
                 | `Constant addr -> E.(constant (L.address addr))
                 | `Admin varname ->
                     add_storage_request varname T.address
                       (L.address "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb") ;
                     E.(storage ~field:varname ()) in *)
              [I.verify E.(sender === compare_to)]
          | _ -> not_implemented "check" in
        let do_action =
          let module VT = Variable.Type in
          match ability with
          | Do_nothing -> []
          | Booemrang -> [I.send_mutez ~destination:E.sender E.amount]
          | Set_delegate ->
              add_parameter_request "delegate" VT.pkh_option ;
              [I.set_delegate E.(parameter ~field:"delegate" ())]
          | Transfer_mutez {amount; destination} ->
              add_parameter_request "amount" VT.mutez ;
              let destination_expr =
                match destination with
                | `Anyone ->
                    add_parameter_request "destination" VT.address ;
                    E.(parameter ~field:"destination" ())
                | `Sender -> E.sender
                | `Address cov ->
                    let get_address =
                      constant_or_variable cov ~make_constant:L.address
                        ~t:T.address
                        ~example:"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" in
                    get_address in
              let amount_expr = E.(parameter ~field:"amount" ()) in
              let check_amount =
                match amount with
                | `No_limit -> []
                | `Limit amount_cov ->
                    let make_constant z =
                      L.mutez (Z.to_string z |> Big_int.big_int_of_string) in
                    [ I.verify
                        E.(
                          amount_expr
                          <== constant_or_variable amount_cov ~make_constant
                                ~t:T.mutez ~example:(Z.of_int 420_000_024)) ]
              in
              check_amount
              @ [I.send_mutez ~destination:destination_expr amount_expr]
          | _ -> not_implemented "ability" in
        let parameter =
          let vt_to_t =
            let open Variable.Type in
            function
            | Mutez -> T.mutez
            | Address -> T.address
            | Nat -> T.nat
            | Key_list -> T.(list key)
            | Signature_option_list -> T.(list (option signature))
            | Bytes -> T.bytes
            | Lambda_unit_to_operation_list -> T.(lambda unit (list operation))
            | Pkh_option -> T.(option key_hash) in
          match !parameter_requests with
          | [] -> T.unit
          | more -> T.comb_record (List.Assoc.map more ~f:vt_to_t) in
        let () =
          (* This is a check that should always succeed unless there is a bug:
             disagreement between this generator and
             `Entrypoint.collect_parameters`. *)
          let other_source = Entrypoint.collect_parameters ep in
          List.iter2 other_source !parameter_requests
            ~f:(fun (namel, typel) (namer, typer) ->
              if String.equal namel namer && Variable.Type.equal typel typer
              then ()
              else
                Failure.raise_textf
                  "BUG: entrypoint parameters mismatch: %s Vs %s (%a)" namel
                  namer Sexp.pp
                  Entrypoint.(sexp_of_t ep) )
          |> List.Or_unequal_lengths.(
               function
               | Ok () -> ()
               | Unequal_lengths ->
                   Failure.raise_textf "BUG: entrypoint %a mismatch length"
                     Sexp.pp
                     Entrypoint.(sexp_of_t ep)) in
        Construct.entry_point name ~parameter (do_check @ do_action) ) in
  Compile.contract entrypoints ~storage_record:!storage_requests

let valid_examples () =
  let alice_pkh = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" in
  let all = ref [] in
  let add name t = all := (name, t) :: !all in
  let open Ability in
  let open Control in
  let open Entrypoint in
  let open Constant_or_variable in
  (* add "empty" [] ; *)
  let default_transfer = make "default" ~control:anyone ~ability:do_nothing in
  add "dead-end" [default_transfer] ;
  add "double-dead-end"
    [default_transfer; make "redefault" ~control:anyone ~ability:do_nothing] ;
  add "boomerang-with-delegation"
    [ make "default" ~control:anyone ~ability:booemrang
    ; make "set_delegate"
        ~control:(sender_is (constant alice_pkh))
        ~ability:set_delegate ] ;
  add "admin-transfer-with-limit"
    [ default_transfer
    ; make "admin_transfer"
        ~control:(sender_is (variable "administrator"))
        ~ability:
          (transfer_mutez
             ~amount:(`Limit (constant (Z.of_int 42_042_042)))
             ~destination:`Anyone ) ] ;
  List.rev !all
