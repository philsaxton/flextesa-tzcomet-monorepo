type t = (int, string) Tezos_micheline.Micheline.node

val of_canonical_micheline : string Tezos_micheline.Micheline.canonical -> t
val to_canonical_micheline : t -> string Tezos_micheline.Micheline.canonical

val of_micheline_node :
  ?make_location:('a -> int) -> ('a, string) Tezos_micheline.Micheline.node -> t

val pp : Format.formatter -> t -> unit

(** Construct Micheline/Michelson expressions. *)
module C : sig
  val int : Z.t -> t
  val string : string -> t
  val bytes : bytes -> t
  val prim : ?annotations:string list -> string -> t list -> t
  val seq : t list -> t
  val t_or : t list -> t
  val t_unit : t
  val t_pair : t list -> t
  val script : parameter:t -> storage:t -> t list -> t
  val concrete : string -> t
end

(** Match on Micheline/Michelson expressions. *)
module M : sig
  type ('l, 'p) node = ('l, 'p) Tezos_micheline.Micheline.node =
    | Int of 'l * Z.t
    | String of 'l * string
    | Bytes of 'l * bytes
    | Prim of
        'l * 'p * ('l, 'p) Tezos_micheline.Micheline.node list * string list
    | Seq of 'l * ('l, 'p) Tezos_micheline.Micheline.node list
end

val encoding : t Data_encoding.t
(** Encoding compatible with the “real” thing; it does not not normalize the the
    reppresentation for the binary serialization to be the same as ["PACK"] or
    ["tezos-client hash data ..."]. *)

val of_json : Ezjsonm.value -> t
val to_json : t -> Ezjsonm.value

val primitives : (string * string) list
(** The Michelson primitives from the protocol, converted to strings. *)

val expr_encoding : string Tezos_micheline.Micheline.canonical Data_encoding.t
