let get_script_field_exn string_micheline field =
  let open Untyped.M in
  let type_opt =
    match string_micheline with
    | Seq (_, l) ->
        Base.List.find_map l ~f:(function
          | Prim (_, f, [t], _) when f = field -> Some t
          | _ -> None )
    | _ -> None in
  match type_opt with
  | None -> Fmt.failwith "Cannot find the %S field for the contract" field
  | Some s -> Untyped.of_micheline_node s

let get_storage_type_exn string_micheline =
  get_script_field_exn string_micheline "storage"

let get_parameter_type_exn string_micheline =
  get_script_field_exn string_micheline "parameter"
