val get_storage_type_exn : Untyped.t -> Untyped.t
(** Find the ["storage"] section of a Micheline-encoded Michelson contract.

    @raise [Failure _\] if not found. *)

val get_parameter_type_exn : Untyped.t -> Untyped.t
(** Find the ["parameter"] section of a Micheline-encoded Michelson contract.

    @raise [Failure _\] if not found. *)
