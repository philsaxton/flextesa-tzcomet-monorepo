(** This module contains partial implementations of ["PACK"], the
    canonicalization of Michelson done in the protocol uses type information. *)

val watermark : string
val pack : Untyped.t -> string
