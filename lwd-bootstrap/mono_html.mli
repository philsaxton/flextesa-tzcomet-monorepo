module H5 = Tyxml_lwd.Html

type 'a t = 'a H5.elt

val t : string -> [> Html_types.txt] t
val empty : unit -> 'a Lwd_seq.t Lwd.t

val ( % ) :
  'a Tyxml_lwd.node Lwd_seq.t Lwd.t -> 'a Tyxml_lwd.node Lwd_seq.t Lwd.t -> 'a t

val ( %% ) :
     ([> Html_types.txt] as 'a) Tyxml_lwd.node Lwd_seq.t Lwd.t
  -> 'a Tyxml_lwd.node Lwd_seq.t Lwd.t
  -> 'a t

val singletize : (?a:'a -> 'b list -> 'c) -> ?a:'a -> 'b -> 'c
val list : 'a t list -> 'a t
val parens : ([> Html_types.txt] as 'a) Tyxml_lwd.node Lwd_seq.t Lwd.t -> 'a t
val hr : ([< Html_types.hr_attrib], [> Html_types.hr]) H5.nullary
val br : ([< Html_types.br_attrib], [> Html_types.br]) H5.nullary

val p :
     ?a:[< Html_types.p_attrib] H5.attrib list
  -> [< Html_types.p_content_fun] H5.elt
  -> [> Html_types.p] H5.elt

val i :
     ?a:[< Html_types.i_attrib] H5.attrib list
  -> [< Html_types.i_content_fun] H5.elt
  -> [> Html_types.i] H5.elt

val b :
     ?a:[< Html_types.b_attrib] H5.attrib list
  -> [< Html_types.b_content_fun] H5.elt
  -> [> Html_types.b] H5.elt

val em :
     ?a:[< Html_types.em_attrib] H5.attrib list
  -> [< Html_types.em_content_fun] H5.elt
  -> [> Html_types.em] H5.elt

val code :
     ?a:[< Html_types.code_attrib] H5.attrib list
  -> [< Html_types.code_content_fun] H5.elt
  -> [> Html_types.code] H5.elt

val span :
     ?a:[< Html_types.span_attrib] H5.attrib list
  -> [< Html_types.span_content_fun] H5.elt
  -> [> Html_types.span] H5.elt

val sub :
     ?a:[< Html_types.sub_attrib] H5.attrib list
  -> [< Html_types.sub_content_fun] H5.elt
  -> [> Html_types.sub] H5.elt

val sup :
     ?a:[< Html_types.sup_attrib] H5.attrib list
  -> [< Html_types.sup_content_fun] H5.elt
  -> [> Html_types.sup] H5.elt

val small :
     ?a:[< Html_types.small_attrib] H5.attrib list
  -> [< Html_types.small_content_fun] H5.elt
  -> [> Html_types.small] H5.elt

val strong :
     ?a:[< Html_types.strong_attrib] H5.attrib list
  -> [< Html_types.strong_content_fun] H5.elt
  -> [> Html_types.strong] H5.elt

val abbr :
     ?a:[< Html_types.abbr_attrib] H5.attrib list
  -> [< Html_types.abbr_content_fun] H5.elt
  -> [> Html_types.abbr] H5.elt

val a :
     ?a:[< Html_types.a_attrib] H5.attrib list
  -> 'a H5.elt
  -> [> 'a Html_types.a] H5.elt

val div :
     ?a:[< Html_types.div_attrib] H5.attrib list
  -> [< Html_types.div_content_fun] H5.elt
  -> [> Html_types.div] H5.elt

val pre :
     ?a:[< Html_types.pre_attrib] H5.attrib list
  -> [< Html_types.pre_content_fun] H5.elt
  -> [> Html_types.pre] H5.elt

val h1 :
     ?a:[< Html_types.h1_attrib] H5.attrib list
  -> [< Html_types.h1_content_fun] H5.elt
  -> [> Html_types.h1] H5.elt

val h2 :
     ?a:[< Html_types.h2_attrib] H5.attrib list
  -> [< Html_types.h2_content_fun] H5.elt
  -> [> Html_types.h2] H5.elt

val h3 :
     ?a:[< Html_types.h3_attrib] H5.attrib list
  -> [< Html_types.h3_content_fun] H5.elt
  -> [> Html_types.h3] H5.elt

val h4 :
     ?a:[< Html_types.h4_attrib] H5.attrib list
  -> [< Html_types.h4_content_fun] H5.elt
  -> [> Html_types.h4] H5.elt

val h5 :
     ?a:[< Html_types.h5_attrib] H5.attrib list
  -> [< Html_types.h5_content_fun] H5.elt
  -> [> Html_types.h5] H5.elt

val h6 :
     ?a:[< Html_types.h6_attrib] H5.attrib list
  -> [< Html_types.h6_content_fun] H5.elt
  -> [> Html_types.h6] H5.elt

val tr :
     ?a:[< Html_types.tr_attrib] H5.attrib list
  -> [< Html_types.tr_content_fun] H5.elt
  -> [> Html_types.tr] H5.elt

val td :
     ?a:[< Html_types.td_attrib] H5.attrib list
  -> [< Html_types.td_content_fun] H5.elt
  -> [> Html_types.td] H5.elt

val th :
     ?a:[< Html_types.th_attrib] H5.attrib list
  -> [< Html_types.th_content_fun] H5.elt
  -> [> Html_types.th] H5.elt

val blockquote :
     ?a:[< Html_types.blockquote_attrib] H5.attrib list
  -> [< Html_types.blockquote_content_fun] H5.elt
  -> [> Html_types.blockquote] H5.elt

val classes : Html_types.nmtokens -> [> `Class] H5.attrib
val style : string -> [> `Style_Attr] H5.attrib
val it : string -> [> Html_types.i] H5.elt
val bt : string -> [> Html_types.b] H5.elt
val ct : string -> [> Html_types.code] H5.elt

val link :
     target:string
  -> ?a:[< Html_types.a_attrib > `Href] H5.attrib list
  -> 'a H5.elt
  -> [> 'a Html_types.a] H5.elt

val url :
     ?a:[< Html_types.a_attrib > `Href] H5.attrib list
  -> (string -> 'a H5.elt)
  -> string
  -> [> 'a Html_types.a] H5.elt

val abbreviation :
  string -> [< Html_types.abbr_content_fun] H5.elt -> [> Html_types.abbr] H5.elt

val button :
     ?a:[< Html_types.button_attrib > `OnClick] H5.attrib list
  -> action:(unit -> unit)
  -> [< Html_types.button_content_fun] H5.elt
  -> [> Html_types.button] H5.elt

val onclick_action : (unit -> unit) -> [> `OnClick] H5.attrib

val itemize :
     ?numbered:bool
  -> ?a_ul:[< Html_types.ul_attrib] H5.attrib list
  -> ?a_li:[< Html_types.li_attrib] H5.attrib list
  -> [< Html_types.li_content_fun] H5.elt list
  -> [> `Ol | `Ul] H5.elt

val details :
     summary:[< Html_types.summary_content_fun] t
  -> [< Html_types.details_content_fun] t
  -> [> Html_types.details] t

val input_bidirectional :
     ?a:[< Html_types.input_attrib > `OnInput `Value] H5.attrib list
  -> string Reactive.Bidirectional.t
  -> [> Html_types.input] H5.elt
