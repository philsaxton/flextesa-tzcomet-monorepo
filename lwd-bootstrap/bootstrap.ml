open Mono_html
module List = ListLabels

let str = Format.asprintf
let kstr = Format.kasprintf
let dbgf fmt = kstr (Printf.eprintf "debug: %s\n%!") fmt

module Label_kind = struct
  type t =
    [ `Primary
    | `Secondary
    | `Success
    | `Danger
    | `Warning
    | `Info
    | `Light
    | `Dark ]

  let to_string : t -> string = function
    | `Primary -> "primary"
    | `Secondary -> "secondary"
    | `Success -> "success"
    | `Danger -> "danger"
    | `Warning -> "warning"
    | `Info -> "info"
    | `Light -> "light"
    | `Dark -> "dark"
end

let label kind content =
  H5.span
    ~a:[classes ["alert"; str "alert-%s" (Label_kind.to_string kind)]]
    [content]

let color kind content =
  H5.span ~a:[classes [str "text-%s" (Label_kind.to_string kind)]] [content]

let muted f content =
  (* Using `?a:Some` to force the type inference to see the optional argument. *)
  f ?a:(Some [classes ["text-muted"]]) content

let spinner ?kind:_ content =
  H5.div
    ~a:[classes ["spinner-border"]; H5.a_role (Lwd.pure ["status"])]
    [H5.span ~a:[classes ["sr-only"]] [content]]

let alert ?(kind = `Primary) content =
  H5.div
    ~a:
      [ classes ["alert"; str "alert-%s" (Label_kind.to_string kind)]
      ; H5.a_role (Lwd.pure ["alert"]) ]
    [content]

let p_lead ?(a = []) c = p ~a:(classes ["lead"] :: a) c
let div_lead ?(a = []) c = div ~a:(classes ["lead"] :: a) c

let bordered ?(a = []) ?(rounded = `Default) ?(kind = `Primary) content =
  let a =
    [ classes
        ( ["border"; str "border-%s" (Label_kind.to_string kind)]
        @ match rounded with `Default -> ["rounded-sm"] | `No -> [] ) ]
    @ a in
  H5.div ~a [content]

let monospace content = H5.span ~a:[classes ["text-monospace"]] [content]
let terminal_logs content = div ~a:[classes ["bg-dark"; "text-white"]] content
let _raw_button = button

let button ?(outline = false) ?(disabled = false) ?(size = `Normal)
    ?(kind = `Light) content ~action =
  let a =
    [ classes
        ( [ "btn"
          ; str "btn-%s%s"
              (if outline then "outline-" else "")
              (Label_kind.to_string kind) ]
        @
        match size with
        | `Normal -> []
        | `Small -> ["btn-sm"]
        | `Large -> ["btn-lg"] ) ]
    @ if disabled then [H5.a_disabled ()] else [] in
  _raw_button ~action ~a content

let close_button ~action =
  let a = [classes ["close"]; H5.a_aria "label" (Lwd.pure ["Close"])] in
  _raw_button ~action ~a (t "×")

let container ?(suffix = "-md") c =
  div ~a:[classes [str "container%s" suffix]] c

let container_fluid c = container ~suffix:"-fluid" c

module Fresh_id = struct
  let _ids = ref 0
  let make prefix = incr _ids ; str "%s-%06d" prefix !_ids
  let of_option prefix = function Some s -> s | None -> make prefix
end

module Dropdown_menu = struct
  let item ~action content = `Menu_item (content, action)
  let header content = `Menu_header content

  let button ?(kind = `Light) ?id content items =
    let id = Fresh_id.of_option "dropdown" id in
    let div_items =
      let open H5 in
      List.map items ~f:(function
        | `Menu_item (content, action) ->
            button
              ~a:
                [ a_class (Reactive.pure ["dropdown-item"])
                ; a_onclick (Tyxml_lwd.Lwdom.attr (fun _ -> action () ; false))
                ]
              [content]
        | `Menu_header content ->
            h6 ~a:[a_class (Reactive.pure ["dropdown-header"])] [content] )
    in
    H5.(
      let open! Tyxml_lwd.Lwdom in
      div
        ~a:
          [ a_class (Reactive.pure ["dropdown"])
          ; a_style (Reactive.pure "display: inline-block") ]
        [ button
            ~a:
              [ a_class
                  (Reactive.pure
                     [ "btn"
                     ; str "btn-%s" (Label_kind.to_string kind)
                     ; "dropdown-toggle" ] )
                (* ; a_type (Reactive.pure `Button) *)
              ; a_id (Reactive.pure id)
              ; a_user_data "toggle" (Reactive.pure "dropdown")
              ; a_aria "haspopup" (Reactive.pure ["true"])
              ; a_aria "expanded" (Reactive.pure ["false"]) ]
            [content]
        ; div
            ~a:
              [ a_class
                  (Reactive.pure ["dropdown-menu"; "dropdown-menu-lg-right"])
              ; a_aria "labelledby" (Reactive.pure [id]) ]
            div_items ])
end

module Navigation_bar = struct
  (* https://getbootstrap.com/docs/4.5/components/navbar/#toggler *)
  let item ?(active = Reactive.pure true) ?fragment c ~action =
    `Item
      (c, (action : unit -> unit), active, (fragment : string Reactive.t option))

  let make ?(aria_label = "Show/Hide Navigation") ?id ~brand items =
    let open H5 in
    let toggler_id = Fresh_id.of_option "dropdown" id in
    let content =
      [ button
          ~a:
            [ classes ["navbar-toggler"; "bg-light"]
            ; a_user_data "toggle" (Reactive.pure "collapse")
            ; a_user_data "target" (kstr Reactive.pure "#%s" toggler_id)
            ; a_aria "controls" (Reactive.pure [toggler_id])
            ; a_aria "expanded" (Reactive.pure ["false"])
            ; a_aria "label" (Reactive.pure [aria_label]) ]
          [span ~a:[classes ["navbar-toggler-icon"]] []]
      ; a
          ~a:
            [ H5.a_style (Reactive.pure "margin: auto")
              (* classes ["navbar-brande"] *) ]
          [brand]
      ; div
          ~a:
            [ classes ["collapse"; "navbar-collapse"]
            ; a_id (Reactive.pure toggler_id) ]
          [ ul
              ~a:[classes ["navbar-nav"; "mr-auto"; "mt-2"; "mt-lg-0"]]
              (List.map items ~f:(function
                   | `Item (content, action, active, fragment) ->
                   (* a_class
                         (Reactive.map
                            (function
                              | true -> ["nav-item"; "active"]
                              | false -> ["nav-item"])
                            active) *)
                   Reactive.bind active ~f:(function
                     | true ->
                         li
                           ~a:[classes ["nav-item"; "active"]]
                           [ a
                               ~a:
                                 ( [classes ["nav-link"]; onclick_action action]
                                 @
                                 match fragment with
                                 | None -> []
                                 | Some frg ->
                                     [a_href (Reactive.map ~f:(str "#%s") frg)]
                                 )
                               [content] ]
                     | false ->
                         li
                           ~a:[classes ["nav-item"]]
                           [a ~a:[classes ["nav-link"]] [content]] ) ) ) ] ]
    in
    nav content
      ~a:
        [ classes ["navbar"; "navbar-expand-lg"; "navbar-light"; "bg-light"]
        ; H5.a_style (Reactive.pure "background-color: #93afdb !important") ]
end

module Tab_bar = struct
  type item =
    { label: Html_types.a_content_fun H5.elt
    ; active: bool Reactive.t
    ; action: unit -> unit }

  let item ~active ~action label = {label; action; active}

  let make items =
    H5.ul
      ~a:[classes ["nav"; "nav-tabs"]]
      (List.map items ~f:(fun {label; active; action} ->
           let li = H5.li ~a:[classes ["nav-item"]] in
           Reactive.bind active ~f:(function
             | true ->
                 li
                   [ a
                       ~a:
                         [ classes ["nav-link"]
                         ; H5.a_onclick
                             (Tyxml_lwd.Lwdom.attr (fun _ev ->
                                  action () ; true ) ) ]
                       label ]
             | false ->
                 (* in bootstrap active means currently already activated *)
                 li [a ~a:[classes ["active"; "nav-link"]] label] ) ) )
end

module Modal = struct
  let ok_button ?(outline = true) ?(size = `Normal) ?(kind = `Primary) content
      ~action =
    H5.(
      let a =
        [ classes
            ( [ "btn"
              ; str "btn-%s%s"
                  (if outline then "outline-" else "")
                  (Label_kind.to_string kind) ]
            @
            match size with
            | `Normal -> []
            | `Small -> ["btn-sm"]
            | `Large -> ["btn-lg"] )
        ; a_user_data "dismiss" (Lwd.pure "modal") ] in
      _raw_button ~action ~a content)

  let mk_modal ~modal_id ~(modal_title : string) ~modal_body ?(ok_text = "Ok")
      ~ok_action () =
    let open! Tyxml_lwd.Lwdom in
    let label_id = "label_id" in
    H5.(
      div
        ~a:
          [ classes ["modal"; "fade"]
          ; a_id (Lwd.pure modal_id)
          ; a_tabindex (Lwd.pure (-1))
          ; a_role (Lwd.pure ["dialog"])
          ; a_aria "labelledby" (Lwd.pure [label_id])
          ; a_aria "hidden" (Lwd.pure ["true"]) ]
        [ div
            ~a:[classes ["modal-dialog"]; a_role (Lwd.pure ["document"])]
            [ div
                ~a:[classes ["modal-content"]]
                [ div
                    ~a:[classes ["modal-header"]]
                    [ h5
                        ~a:[classes ["modal-title"]; a_id (Lwd.pure label_id)]
                        [t modal_title]
                    ; button
                        ~a:
                          [ classes ["close"]
                          ; a_aria "label" (Reactive.pure ["Close"])
                          ; a_user_data "dismiss" (Lwd.pure "modal") ]
                        [ span
                            ~a:[a_aria "hidden" (Reactive.pure ["true"])]
                            [t ""] ] ]
                ; div ~a:[classes ["modal-body"]] [modal_body]
                ; div
                    ~a:[classes ["modal-footer"]]
                    [ ok_button (t ok_text) ~action:ok_action
                    ; button
                        ~a:
                          [ classes ["btn"; "btn-secondary"]
                          ; a_user_data "dismiss" (Lwd.pure "modal") ]
                        [t "Close"] ] ] ] ])
end

module Form = struct
  module Item = struct
    type input =
      { label: Html_types.label_content_fun H5.elt option
      ; id: string option
      ; active: bool Reactive.t
      ; help: Html_types.small_content_fun H5.elt option }

    type t =
      | Row of (int * t) list
      | Any of Html_types.div_content_fun H5.elt
      | Input of
          { input: input
          ; placeholder: string Reactive.t option
          ; content: string Reactive.Bidirectional.t }
      | Check_box of {input: input; checked: bool Reactive.Bidirectional.t}
      | Button of
          { label: Html_types.button_content_fun H5.elt
          ; active: bool Reactive.t
          ; action: unit -> unit }

    let rec to_div ?(enter_action = fun () -> ()) ?cols =
      let open H5 in
      let generic_input ~active ?id ?help ?placeholder ~kind lbl more_a =
        let the_id = Fresh_id.of_option "input-item" id in
        let help_id = the_id ^ "Help" in
        let full_label =
          match lbl with
          | None -> empty ()
          | Some lbl ->
              label
                ~a:
                  [ (* shoud be for="<id>" *)
                    classes
                      ( match kind with
                      | `Text -> []
                      | `Checkbox -> ["form-check-label"] ) ]
                [lbl] in
        let full_input =
          let a_base =
            [ classes
                [ ( match kind with
                  | `Text -> "form-control"
                  | `Checkbox -> "form-check-input" ) ]
            ; a_id (Reactive.pure the_id)
            ; a_aria "describedBy" (Reactive.pure [help_id])
            ; a_onkeydown
                (Tyxml_lwd.Lwdom.attr (fun ev ->
                     (* Printf.eprintf "keycode: %d\n%!" ev##.keyCode ; *)
                     match ev##.keyCode with
                     | 13 when not (Js_of_ocaml.Js.to_bool ev##.shiftKey) ->
                         enter_action () ; false
                     | _ -> true ) )
            ; a_input_type (Reactive.pure kind) ]
            @ ( match placeholder with
              | None -> []
              | Some plc -> [a_placeholder plc] )
            @ more_a in
          Reactive.bind active ~f:(function
            | true -> input ~a:a_base ()
            | false -> input ~a:(a_disabled () :: a_base) () ) in
        let full_help =
          small
            ~a:
              [a_id (Reactive.pure help_id); classes ["form-text"; "text-muted"]]
            (match help with None -> [] | Some h -> [h]) in
        let div_content =
          match kind with
          | `Text -> [full_label; full_input; full_help]
          | `Checkbox -> [full_input; full_label; full_help] in
        let cols_class =
          match cols with
          | None -> []
          | Some n when 1 <= n && n <= 12 -> [str "col-md-%d" n]
          | Some _ -> assert false in
        let div_classes =
          match kind with
          | `Text -> cols_class
          | `Checkbox -> "form-check" :: cols_class in
        div ~a:[classes ("form-group" :: div_classes)] div_content in
      function
      | Row l ->
          div
            ~a:[classes ["form-row"]]
            (List.map l ~f:(fun (cols, item) ->
                 to_div ~enter_action ~cols item ) )
      | Input {input= {label= lbl; id; help; active}; placeholder; content} ->
          generic_input ?id ?help ~kind:`Text lbl ~active ?placeholder
            [ a_value (Reactive.Bidirectional.get content)
            ; a_oninput
                (Tyxml_lwd.Lwdom.attr
                   Js_of_ocaml.(
                     fun ev ->
                       Js.Opt.iter ev##.target (fun input ->
                           Js.Opt.iter (Dom_html.CoerceTo.input input)
                             (fun input ->
                               let v = input##.value |> Js.to_string in
                               (* dbgf "TA inputs: %d bytes: %S" (String.length v) v ; *)
                               Reactive.Bidirectional.set content v ) ) ;
                       true) ) ]
      | Check_box {input= {label= lbl; id; help; active}; checked} ->
          Reactive.Bidirectional.get checked
          |> Reactive.bind ~f:(fun init_checked ->
                 let initstatus = if init_checked then [a_checked ()] else [] in
                 generic_input ?id ?help ~kind:`Checkbox lbl ~active
                   ( initstatus
                   @ [ a_onclick
                         (Tyxml_lwd.Lwdom.attr
                            Js_of_ocaml.(
                              fun ev ->
                                Js.Opt.iter ev##.target (fun input ->
                                    Js.Opt.iter (Dom_html.CoerceTo.input input)
                                      (fun input ->
                                        let v = input##.checked |> Js.to_bool in
                                        (* dbgf "checkbox → %b" v ; *)
                                        Reactive.Bidirectional.set checked v ) ) ;
                                true) ) ] ) )
      | Button {label= lbl; active; action} ->
          let btn = ["btn"; "btn-primary"] in
          let cls =
            match cols with
            | None -> fun x -> x
            | Some n -> fun x -> div ~a:[classes [str "col-md-%d" n]] [x] in
          Reactive.bind active ~f:(fun is_active ->
              let a =
                let base =
                  [a_button_type (Reactive.pure `Submit); classes btn] in
                match is_active with
                | true ->
                    a_onclick (Tyxml_lwd.Lwdom.attr (fun _ -> action () ; false))
                    :: base
                | false -> a_disabled () :: base in
              button ~a [lbl] |> cls )
      | Any the_div ->
          let cls =
            let base = ["form-group"] in
            match cols with None -> base | Some n -> str "col-md-%d" n :: base
          in
          div ~a:[classes cls] [the_div]
  end

  open Item

  let input ?(active = Reactive.pure true) ?id ?placeholder ?help ?label content
      =
    Input {input= {label; id; help; active}; placeholder; content}

  let check_box ?(active = Reactive.pure true) ?id ?help ?label checked =
    Check_box {input= {label; id; help; active}; checked}

  let submit_button ?(active = Reactive.pure true) label action =
    Button {action; active; label}

  let cell i item = (i, item)
  let row l = Row l
  let magic d = Any d

  let make ?enter_action items =
    let div_items = List.map ~f:(Item.to_div ?enter_action) items in
    H5.form div_items
end

module Collapse = struct
  type state = [`Hidden | `Hiding | `Shown | `Showing]

  module Global_jquery_communication = struct
    (** This module brings the ["*.bs.collapse"] jQuery events into the Lwd
        realm, using each a global handler for traditional JS events.

        See the generation of the events forwarding in ["src/gen-web/main.ml"]:

        {v
           $(document).on('hidden.bs.collapse', function (e) {
              var ev = new CustomEvent('collapse-hidden', { detail: e.target.id } );
              document.body.dispatchEvent(ev);
           })
        v} *)

    let done_once = ref false
    let ids_and_states : (string * state Reactive.var) list ref = ref []

    let ensure_handlers () =
      match !done_once with
      | true -> ()
      | false ->
          let open Js_of_ocaml in
          (* This adds one listener per collapse element … *)
          let the_div =
            (Dom_html.document##.body :> Js_of_ocaml.Dom_html.element Js.t)
          in
          List.iter
            [ ("shown", `Shown)
            ; ("show", `Showing)
            ; ("hide", `Hiding)
            ; ("hidden", `Hidden) ]
            ~f:(fun (evname, resulting_status) ->
              let ev_type = kstr Dom.Event.make "collapse-%s" evname in
              let _id =
                Dom_html.addEventListener the_div ev_type
                  (Dom_html.handler (fun ev ->
                       dbgf "html-handler (%d): %s -- %s"
                         (List.length !ids_and_states)
                         (Js.to_string ev##._type)
                         (Js.to_string ev##.detail) ;
                       List.iter !ids_and_states ~f:(fun (the_id, state) ->
                           if String.equal (Js.to_string ev##.detail) the_id
                           then Reactive.set state resulting_status ) ;
                       Js._true ) )
                  Js._true in
              () ) ;
          done_once := true

    let register id state = ids_and_states := (id, state) :: !ids_and_states
    let unregister id = ids_and_states := List.remove_assoc id !ids_and_states
    (*  ~equal:String.equal *)
  end

  type t = {id: string Reactive.t; state: state Reactive.var}

  let make ?id () =
    let (state : state Reactive.var) = Reactive.var `Hidden in
    let the_id_prim =
      Reactive.prim
        ~acquire:(fun _ ->
          let the_id = Fresh_id.of_option "collapse" id in
          Global_jquery_communication.ensure_handlers () ;
          Global_jquery_communication.register the_id state ;
          the_id )
        ~release:(fun _ id -> Global_jquery_communication.unregister id) in
    let the_id = Reactive.get_prim the_id_prim in
    {id= the_id; state}

  let full_state t = Reactive.get t.state

  let collapsed_state t =
    full_state t
    |> Reactive.map ~f:(function
         | `Hiding | `Hidden -> true
         | `Showing | `Shown -> false )

  let make_button ?(kind = `Primary) ?style ?more_classes:_ t content =
    let more_a = match style with None -> [] | Some s -> [H5.a_style s] in
    H5.button
      ~a:
        ( more_a
        @ H5.
            [ classes
                [ "btn"
                ; "btn-sm"
                ; str "btn-outline-%s" (Label_kind.to_string kind) ]
            ; a_user_data "toggle" (Lwd.pure "collapse")
            ; a_user_data "target" (Reactive.map ~f:(str "#%s") t.id)
            ; a_aria "expanded"
                (Reactive.map (collapsed_state t) ~f:(function
                  | true -> ["false"]
                  | false -> ["true"] ) )
            ; a_aria "controls" (Reactive.map ~f:(fun x -> [x]) t.id) ] )
      [content]

  let make_div t content =
    Reactive.bind (collapsed_state t) ~f:(function
      | true -> div ~a:[classes ["collapse"]; H5.a_id t.id] (empty ())
      | false ->
          div ~a:[classes ["collapse"; "show"]; H5.a_id t.id] (content ()) )

  let fixed_width_reactive_button_with_div_below ?kind t ~width ~button content
      =
    make_button ?kind t
      ~style:(Reactive.pure (str "width: %s" width))
      (Reactive.bind (collapsed_state t) ~f:button)
    % make_div t content
end

module Table = struct
  let simple ?header_row content =
    let open H5 in
    let thead =
      match header_row with
      | None -> None
      | Some hl -> Some (thead [tr (List.map hl ~f:(fun x -> th [x]))]) in
    tablex
      ~a:[classes ["table"; "table-bordered"; "table-hover"]]
      ?thead
      [tbody [content]]
end
